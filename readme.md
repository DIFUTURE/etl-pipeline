# An Agile ETL Pipeline for i2b2 and tranSMART

This project provides a flexible pipeline for rapidly creating instances of i2b2 and tranSMART containing clinical data.

## Overview

The core design goals are:
1. Input flexibility: the tool is able to directly ingest data that has been exported from typical clinical and research systems.
2. Output flexibility: based on one single configuration, the tool is able to load data into i2b2 and tranSMART.
3. Declarative transformation: the mapping of export files to ontology trees in i2b2 or tranSMART is performed in a declarative manner.
4. Automatism: the tool automates tedious tasks, such as detecting and fixing charsets, spelling errors, date formats, missing data etc.

On the implementation level, this tool follows a "meta-ETL" approach. This means that data is transformed into staging files required
by common ETL tools for i2b2 or tranSMART so that these solutions can be used for loading the data. In addition, functionality
has been implemented to create and load ontology trees. Currently, the following backends are supported:
1. transmart-batch for tranSMART
2. transmart-batch for i2b2
3. tMDataLoader for tranSMART

## System requirements
* Java 8
* Maven 3

## Assumptions
In its current form, the tool makes the following assumptions about the data:
1. Input data is provided as a set of CSV files
2. In each file, each line represents data for one patient (there may be multiple lines per patient)
3. In each file, there exist one or multiple 
    * identifiers for assigning the data to the patient
    * identifiers for assigning the data to an encounter or visit of the patient
    * timestamps for assigning the data to an encounter or visit of the patient     
4. One of the file contains basic patient master data and each patient has a unique (composite) identifier
5. Optional: one file contains basic data about visits / encounters
6. Optional: one file contains the mapping between patients and visits / encounters

## Basic configuration

TODO: Describe very basic example

```Java

##########################################
# Subject                                #
##########################################
Subject.File=Patients.csv 
Subject.Subject=Patient
Subject.Age=CALCULATE
Subject.BirthDate=Geburtsdatum
Subject.DateFormat=dd.MM.yyyy
Subject.Sex=Geschlecht
Subject.Sex.Female=2
Subject.Sex.Male=1
Subject.Sex.Unknown=3

##########################################
# Encounter                              #
##########################################
Encounter.File=Encounters.csv
Encounter.Encounter=Fall
Encounter.Start=Aufnahmedatum
Encounter.End=Entlassdatum
Encounter.DateFormat=dd.MM.yyyy

##########################################
# Mapping                                #
##########################################
Mapping.File=Encounters.csv
Mapping.Subject=Patient
Mapping.Encounter=Fall

##########################################
# Entity                                 #
##########################################
File6r=Scores.csv
File6r.Name=Score
File6r.Encounter=Fall
File6r.Path=Clinical Scores

File6r.Start=ZNLMSVKLDA
File6r.End=ZNLMSVKLDA
File6r.DateFormat=dd.MM.yyyy

File6r.Concept=PASAT Points
File6r.Whitelist.0=PASAT Points
File6r.Whitelist.1=Canceled

File6r.Mapping.ZNLMSVK281=PASAT Points
File6r.Mapping.ZNLMSVKL29=Canceled
```

## Execution
The program expects at least three arguments.
1. The type of the target instance (designated with `-t`). Can be one of:
    * ```tm``` For tranSMART instances.
    * ```i2b2``` For i2b2 instances.
2. Absolute or relative path to the folder containing the input files as well as configuration files (designated with `-f`)
3. The working mode of the program (designated with `-m`). Can be one of:
    * ```stage``` Create directory with staging files
    * ```unstage``` Remove directory with staging files
    * ```load``` Load data into the destination instance
    * ```delete``` Delete data from the destination instance
4. The type of loading tool to use. Per default, transmart-batch will be used. To use tMDataLoader use:
	* ```--loadingtool tmdl```
	* ```-l tmdl```
5. Additional parameters can be used to specify the credentials needed to access the underlying databases.

Example: `mvn spring-boot:run -Dspring-boot.run.arguments="-f example/official/ -t i2b2 -m stage" && mvn spring-boot:run -Dspring-boot.run.arguments="-f example/official/ -t i2b2 -m load"`

## Features that make your life easier

### Automated merging of data from multiple files

Data for the same entities distributed over multiple files can be merged automatically. Example:

```
File2=Lab_2019.csv
File2.Append.0=Lab_2018.csv
File2.Append.1=Lab_2017.csv
File2.Append.2=Lab_2016.csv
```

### Automated detection of charsets
 
The tool can ingest files that have been encoded using different charsets. This even works when data for the same entity
is distributed over multiple files. This feature is enabled by default; there is no configuration option. 
 
### Automated detection of CSV syntax
 
The tool can parse files that have been encoded using different CSV syntaxes. This even works when data for the same entity
is distributed over multiple files. This feature is enabled by default; there is no configuration option. 
 
### Automated detection of data types
 
Data types (e.g. decimals with different formats, dates with different formats, numeric vs. text) will be detected automatically.
Values that do not conform to the detected data type will not be loaded. If this doesn't work as expected, configuration
options can be used to overwrite the detection mechanism:

```
Number.Format=#####,###
Number.Locale=DE
FileX.DateFormat
```

Timestamps can be specified using different granularities, e.g.

```
FileX.Start.Day=<Day>
FileX.Start.Month=<Month>
FileX.Start.Year=<Year>
FileX.Start=<Date>
```

### Ignores empty fields
 
Files loaded via the tool may contain an arbitrary number of empty cells. These are simply ignored during loading.
 
### Automatically derives ontology trees from the data
 
For i2b2, the tool will automatically derive ontology trees and store them in XML files during staging. When loading data,
these ontologies will first be loaded into i2b2 and the facts will be loaded in such a manner that the corresponding
concepts are referenced correctly.

### Mapping of column names

Example:
```
File6r.Mapping.ZNLMSVK281=PASAT Points
File6r.Mapping.ZNLMSVKL29=Canceled
```
 
### Handling duplicate column names
 
The tool is able to handle input files with repeating column names. You can refer to the first instance of a column names "X" by X,
to the second instance by "X-2", the third instance by "X-3" and so forth. The order is determined from left to right.
 
### Automated calculation of the age of subjects
 
If only a date-of-birth is available, the age of subjects can be calculated automatically based on the time difference to the
first encounter. Use ```Subject.Age=CALCULATE``` to enable this feature.

### Automated matching of data points to encounters / visits

To match data points to visits using time stamps use ```File1.Encounter=MatchByTimestamp```.

```Encounter=MapByTimestamp``` will map to an encounter that completely contains the observation (start & end)
```Encounter=MapByTimestampOneToMany``` will map to any encounter that contains the observation's start date

### Automated detection and removal of duplicate data

For each numerical concept, each attribute value may only exist once per visit.
For each categorical concept, each attribute value may exist several times, as long as they are all different
from each other. Duplicates will be removed automatically. The duplicates to be removed are selected at random.
A log information regarding how many items were ignored due to non duplicates informs the user.

### Discretization of time series data

The set of visits/encounters can be discretized into a user-specified pattern. Time patterns are specified relative to the
start of the first visit. Only visits that fit into this schemes will be loaded. The following code specifies a pattern consisting of two visits, covering
month 0 to 6 (exclusive) and 6 to 12 (exclusive).

```
Visit.Select.0.Start=0
Visit.Select.0.End=6
Visit.Select.0.Name=HY-1
```

```
Visit.Select.1.Start=6
Visit.Select.1.End=12
Visit.Select.1.Name=HY-2
```

There are two additional options: 
1. The following option can be used to indicate that visits that fall into a single pattern should be merged: ```Visit.Select.Merge=Y``` 
2. The following option can be used to specify that only subjects for which a visit exists for every pattern should be loaded: ```Visit.Select.AllVisitsRequired=Y```

### Filtering of data
 
 Only a specific subset of the records of an input file can be loaded by using the following option:
 ```
 File.Filter.Column=X
 File.Filter.Value=Y
 ```
 As a result, only rows in which the value of attribute X is equal to Y will be icluded in the staging files.
 
### Extracting data from an entity-attribute-value (EAV) schema
 
The tool can be used to normalize data that is stored in an EAV schema. Laboratory values are a typical example:

| Parameter  | Unit     | Norm range | Value |
| ---------- | -------- | ---------- | ----- |
| LDL        | mmol/l   | <160       | 140   |
| Billirubin | µmol/l   | 2-21       | 10    |

With the following code, entities consisting of the combination of parameter and unit can be created with the value as associated concept and the norm range as metadata:

```
# Make sure, we only extract the relevant columns
File.Whitelist.0=Parameter
File.Whitelist.1=Unit
File.Whitelist.2=Norm range
File.Whitelist.3=Value

# Turn this into an EAV entity
File.EAV.Entity=Parameter+Unit
File.EAV.Value=Value
```
With this code, the value will be turned into the concept. Any other attribute will be loaded as metadata (modifier).

```TODO: Describe option EAV.Explicit=Y/N```

### Integrated pseudonymization

The ETL pipeline can be configured to automatically pseudonymize the IDs of patients and visits by accessing the
DIFUTURE Trust Center infrastructure. For this purpose, the following configuration options are provided:

```
Pseudonymization.Enabled=Y
Pseudonymization.PsnURL=<Enter URL of TC interface>
Pseudonymization.AuthURL=<Enter URL of authorization server>
Pseudonymization.AuthClient=<Enter name of client used for authorization>
Pseudonymization.AuthClientSecret=<Enter secret of client>
```

It is required that properties of HL7 FHIR identifiers are specified:

```
Subject.Source=IS-H
Subject.Use=OFFICIAL

Encounter.Source=IS-H
Encounter.Use=OFFICIAL
```

The client secret can also be specified via the command line ```option "-s" or "--secret"```.

### Automated loading of subject IDs

The IDs provided for subjects (or pseudonymized in the pipeline) can be made available for direct access through
the frontends using the option ```Subject.LoadID```.

### Loading of data without visits

Data points can also be directly linked to patients and information on encounters / visits is optional. 

### Structuring of data

The data for the individual entities can be organized in an hierarchical structure, by specifying a path for an entity:

```
EntityX.Path=Root+Level1+Level2
```

TODO: For i2b2, concatenation by "+" is currently not supported.

### Automated analysis of the reliability of timestamps

The tools can read data with timestamps of varying granularity. For some data points, this may be years, for other specific days.
The data will be loaded into the target system independently of these granularites. The reliability can be measured and can
be made available to users through the following option: ```FileX.MeasureTimestampsReliability=Y```.

### Keep or remove subjects without data
 
Property ```Subject.KeepAll```: if set to "Y", also subjects without visits or other data will be loaded into the target system.

### Value mapping

Of course, value mapping is also supported. Example:

```
ENTITY_PREFIX.ValueMapping.VMAPPING_PREFIX.Attribute=ATTRIBUTE_NAME
ENTITY_PREFIX.ValueMapping.VMAPPING_PREFIX.VALUE_1=MAPPED_VALUE_1
ENTITY_PREFIX.ValueMapping.VMAPPING_PREFIX.VALUE_2=MAPPED_VALUE_2
```

You can also perform substring matches. If multiple values match, an arbitary match will be used:
```
ENTITY_PREFIX.ValueMapping.VMAPPING_PREFIX.Wildcard.VALUE_3=MAPPED_VALUE_3
```

Optionally you can use specify a value that should be inserted in case no match is found.
If ```NonMatch``` has not been specified, non-matching values will be kept as-is:

```
ENTITY_PREFIX.ValueMapping.VMAPPING_PREFIX.NonMatch=DEFAULT_VALUE
```

### Summary database

The tool can also create a summary database for tranSMART, e.g. to summarize longitudinal data in a form that it can be analyzed
graphically in the system. Use ```TranSMART.Summary=Y```. For each longitudinal item, the latest, oldest, minimum, maximum etc. will
loaded into the database. If only a single value exists for attributes of an entity, summarization can be disabled
by specifying ```FileX.NoSummary=Y```.

## Notes

When loading data into i2b2 using the transmart-batch backend, transmart-batch will print a lot of warnings about
duplicate I2b2MappingEntries (DuplicationDetectionProcessor). This warning is misleading, the entries are really needed.

### Support timestamps for tranSMART with tMDataLoader

The tool tMDataLoader supports the usage of timestamps, and you can configure component-etl to write timestamps in the
staging files in the format as required by tMDataLoader.

Property ```TranSMART.TMDL.UseTimestamps```: if set to "Y", it is checked for each concepts, whether there are proper timestamps availabe,
and if so, they will be written into the staging file.

You can use property UseTimestamps for each entity, too. 

As tMDataLoader seems to support Timestamps for numerical concepts only, timestamps for categorical concepts are ignored.

The date of the first visit is used as baseline. If there was no date for the first visit found, current date-time is used, so the timestamp may be
a negative one.


## The following features are currently not supported
- Word mapping
- Cross study concepts for tranSMART
- Concept tags
- Reserved attributes forRace, Vital Status, Income in i2b2
- Modifiers for tranSMART