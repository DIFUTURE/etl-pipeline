/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.read;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationSubject;
import org.difuture.components.i2b2transmart.model.AttributeAge;
import org.difuture.components.i2b2transmart.model.Key;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Timestamp;
import org.difuture.components.util.LoggerUtil;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Reads subjects
 * 
 * @author Claudia Lang
 */
public class ReadTaskletSubjectsItem implements Tasklet {

    private int indexSex;
    private int indexZip;
    private int indexAge;
    private int indexBirthYear;
    private int indexBirthMonth;
    private int indexBirthDate;

    // i2b2 specific attributes
    private int indexVitalStatus;
    private int indexLanguage;
    private int indexRace;
    private int indexMaritalStatus;
    private int indexReligion;
    private int indexStatecityzipPath;
    private int indexIncome;
    private int indexBlob;

    @Override
    public RepeatStatus execute(StepContribution contribution,
                                ChunkContext chunkContext) throws Exception, IOException {
        LoggerUtil.info("Reading subject data", this.getClass());

        // Prepare
        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();

        // Read csv file
        Set<String> columns = config.getSelectedColumns();
        List<String[]> rows = CSVReader.getRows(null, columns, config.file);

        handleFirstLineRead(rows.get(0));

        // i is 1 because the first line contains the header and is to skip
        for (int i = 1; i < rows.size(); i++) {
            createSubject(rows.get(i));
        }

        LoggerUtil.info(" - Found " + Model.get().getNumSubjects() + " subjects", this.getClass());

        return RepeatStatus.FINISHED;
    }

    private void createSubject(String[] subjectString) {
        // Prepare
        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();
        List<String> cells = Arrays.asList(subjectString);

        // Key
        Key subjectID = config.key.getKey(cells);

        String sex = null;
        if (indexSex != -1) {
            sex = cells.get(indexSex);
            if (Model.get().isI2b2()) {
                if (sex.equals(config.sexFemale)) {
                    sex = "DEM|SEX:f";
                } else if (sex.equals(config.sexMale)) {
                    sex = "DEM|SEX:m";
                } else if (sex.equals(config.sexUnknown)) {
                    sex = "DEM|SEX:u";
                } else {
                    sex = "DEM|SEX:@";
                }
            }
        }

        String zip = indexZip != -1 ? cells.get(indexZip) : null;
        AttributeAge age = new AttributeAge();
        if ((config.age != null && !config.age.equals("CALCULATE"))) {
            age.setValue(cells.get(indexAge));
        }
        Integer birthYear = null;
        if (config.birthYear != null && !(cells.get(indexBirthYear).equals(""))) {
            birthYear = Integer.parseInt(cells.get(indexBirthYear));
        }
        Integer birthMonth = null;
        if (config.birthMonth != null && !(cells.get(indexBirthMonth).equals(""))) {
            birthMonth = Integer.parseInt(cells.get(indexBirthMonth));
        }
        LocalDate birthDate = null;
        if (config.birthDate != null && !(cells.get(indexBirthDate).equals(""))) {
            birthDate = new Timestamp(Model.get()
                                           .getConfig()
                                           .getSubjectConfiguration()
                                           .getDateFormatter(),
                                      cells.get(indexBirthDate),
                                      true).toLocalDate();
        }

        if (Model.get().isI2b2()) {
            // i2b2 specific attributes
            String vitalStatus = indexVitalStatus != -1 ? cells.get(indexVitalStatus) : null;
            String language = indexLanguage != -1 ? cells.get(indexLanguage) : null;
            String race = indexRace != -1 ? cells.get(indexRace) : null;
            String maritalStatus = indexMaritalStatus != -1 ? cells.get(indexMaritalStatus) : null;
            String religion = indexReligion != -1 ? cells.get(indexReligion) : null;
            String statecityzipPath = indexStatecityzipPath != -1 ? cells.get(indexStatecityzipPath)
                    : null;
            Integer income = indexIncome != -1 ? Integer.parseInt(cells.get(indexIncome)) : null;
            String blob = indexBlob != -1 ? cells.get(indexBlob) : null;

            // Create object for I2B2
            Model.get().createSubject(subjectID,
                                      sex,
                                      zip,
                                      age,
                                      birthYear,
                                      birthMonth,
                                      birthDate,
                                      vitalStatus,
                                      language,
                                      race,
                                      maritalStatus,
                                      religion,
                                      statecityzipPath,
                                      income,
                                      blob);
        } else {
            // Create object for tranSMART
            Model.get().createSubject(subjectID, sex, zip, age, birthYear, birthMonth, birthDate);
        }
    }

    private void handleFirstLineRead(String[] line) throws IOException {
        List<String> headers = Arrays.asList(line);
        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();

        // Extract indexes
        indexSex = headers.indexOf(config.sex);
        indexZip = headers.indexOf(config.zip);
        indexAge = headers.indexOf(config.age);
        indexBirthYear = headers.indexOf(config.birthYear);
        indexBirthMonth = headers.indexOf(config.birthMonth);
        indexBirthDate = headers.indexOf(config.birthDate);

        // i2b2 specific attributes
        if (Model.get().isI2b2()) {
            indexVitalStatus = headers.indexOf(config.vitalStatus);
            indexLanguage = headers.indexOf(config.language);
            indexRace = headers.indexOf(config.race);
            indexMaritalStatus = headers.indexOf(config.maritalStatus);
            indexReligion = headers.indexOf(config.religion);
            indexStatecityzipPath = headers.indexOf(config.stateCityZipPath);
            indexIncome = headers.indexOf(config.income);
            indexBlob = headers.indexOf(config.blob);
        }

        // Extract index for the keys.
        try {
            config.key.calculateIndexes(headers);
        } catch (Exception e) {
            LoggerUtil.fatalUnchecked("File in question: " + config.file.getName(),
                                      e,
                                      this.getClass(),
                                      IllegalStateException.class);
        }
    }
}
