/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.io.File;
import java.util.List;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

/**
 * Writes the mapping file
 * 
 * @author Claudia Lang
 * 
 */
public abstract class WriteTaskletMappingFile implements Tasklet, InitializingBean {

    /** Folder in which to write the mapping file. */
    private File   folder;

    /** The file name for the output file. */
    private String outputFileName;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(folder, "No folder for the mapping file to write available!");
        Assert.notNull(outputFileName, "No file name for the mapping file available!");
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution,
                                ChunkContext chunkContext) throws Exception {

        String fileName = folder.getAbsolutePath() + "/" + outputFileName;

        // Prepare csv writer
        CSVWriter csvWriter = new CSVWriter(fileName);
        csvWriter.open(getHeader());

        // Write rows
        for (String[] row : getEntriesToMappingFile()) {
            csvWriter.write(row);
        }

        // Close csv writer
        csvWriter.close();

        return RepeatStatus.FINISHED;
    }

    /**
     * Sets the file name.
     *
     * <BR>
     * TODO why do we need this method? Why not set this.fileName from within
     * the constructor?
     * 
     * @param fileName
     *            the new file name
     */
    public void setFileName(String outputFileName) {
        this.outputFileName = outputFileName;
    }

    /**
     * Sets the folder.
     *
     * <BR>
     * TODO why do we need this method? Why not set this.folder from within the
     * constructor? Does spring batch supports default constructor only?
     * 
     * @param folder
     *            the new folder
     */
    public void setFolder(File folder) {
        this.folder = folder;
    }

    /**
     * Gets all rows to write in the mapping file
     * 
     * @return a list of string arrays
     */
    protected abstract List<String[]> getEntriesToMappingFile();

    /**
     * Returns the header for the mapping file
     * 
     * @return a string array
     */
    protected abstract String[] getHeader();

}
