/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;
import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFileEAV;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.springbatch.tasklet.read.CSVReader;
import org.difuture.components.i2b2transmart.springbatch.tasklet.read.CSVReader.CanonicalString;
import org.difuture.components.util.LoggerUtil;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Creates entities generically, if the property for EAV is used.
 * 
 * @author Claudia Lang
 * @author Fabian Prasser
 *
 */
public class TaskletCreateGenericEntities implements Tasklet {

    /**
     * Internal class for holding data about a single entity
     * 
     * @author Fabian Prasser
     */
    private static class EAVEntity {

        /** Name of the entity */
        private CanonicalString         name;
        /** Associated filters */
        private final Set<List<String>> filters = new HashSet<>();

        /**
         * Creates a new instance
         * 
         * @param name
         * @param combination
         */
        private EAVEntity(CanonicalString name, List<String> combination) {
            this.name = name;
            this.filters.add(combination);
        }

        /**
         * Updates the entry with new data
         * 
         * @param name
         * @param combination
         */
        public boolean update(CanonicalString name, List<String> combination) {

            // Add filter
            this.filters.add(combination);

            // See if the new name is better
            if (this.name.getCharacterBadness() < name.getCharacterBadness()) { return false; }

            // It is, replace
            this.name = name;
            return true;
        }
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution,
                                ChunkContext chunkContext) throws Exception {

        // Log
        LoggerUtil.info("Processing EAV entities", this.getClass());

        // Prepare
        List<ConfigurationInputFile> newConfigs = new ArrayList<>();

        // For each input file
        Iterator<ConfigurationInputFile> iter = Model.get().getConfig().getFiles().iterator();
        while (iter.hasNext()) {

            // Get
            ConfigurationInputFile config = iter.next();

            // If the property eav has been configured
            if (config.isEAV()) {

                // Log
                LoggerUtil.info(" - Current entity: " + config.getName(), this.getClass());

                // Remove
                iter.remove();

                // If the input file has the property eav, get it.
                ConfigurationInputFileEAV eav = config.getEAV();

                // Get the different combinations of the values of attributes
                // configured as the reference for the entity
                Set<List<String>> combinations = CSVReader.getDistinctValues(config.getMapping(),
                                                                             eav.getEntity(),
                                                                             config.file,
                                                                             config.charset);

                // Log
                LoggerUtil.info(" - Combinations found in file: " + combinations.size(),
                                this.getClass());

                // Fix data quality issues
                Map<CanonicalString, EAVEntity> entities = getEntities(combinations);

                // Log
                LoggerUtil.info(" - Canonical combinations identified: " + entities.size(),
                                this.getClass());

                // For each different value
                for (EAVEntity entity : entities.values()) {

                    // Create a new input file (entity generic entity)
                    newConfigs.add(config.createEAVEntity(eav,
                                                          entity.name.getSafeString(),
                                                          entity.filters));

                }
            }
        }

        // Add the new entities to the existing ones
        Model.get().getConfig().getFiles().addAll(newConfigs);
        LoggerUtil.info(" - Number of generic entities created: " + newConfigs.size(),
                        this.getClass());

        // Exit
        return RepeatStatus.FINISHED;
    }

    /**
     * Extracts a set of canonical forms to resolve data quality issues. We
     * consolidate the combinations that we have found, by comparing them while
     * ignoring certain characters.
     * 
     * @param combinations
     * @return
     */
    private Map<CanonicalString, EAVEntity> getEntities(Set<List<String>> combinations) {

        // Prepare
        Map<CanonicalString, EAVEntity> result = new HashMap<>();

        // For each combination of strings
        for (List<String> combination : combinations) {

            // Get the canonical entity name
            CanonicalString name = CSVReader.getCanonicalForm(getEntityName(combination));

            // Query the map
            EAVEntity entity = result.get(name);

            // Insert missing entry
            if (entity == null) {
                result.put(name, new EAVEntity(name, combination));

                // Update existing entry
            } else {
                if (entity.update(name, combination)) {
                    // Needed to handle transitivity of matching
                    result.remove(name);
                    result.put(name, entity);
                }
            }
        }

        // Done
        return result;
    }

    /**
     * Returns the name for the entity
     * 
     * @param values
     * @return
     */
    private String getEntityName(List<String> combination) {

        // Prepare
        StringBuilder result = new StringBuilder();

        // Add entity
        result.append(combination.get(0));

        // Add metadata
        if (combination.size() > 1) {
            result.append(" (");
        }
        for (int i = 1; i < combination.size(); i++) {
            result.append(combination.get(i));
            if (i != combination.size() - 1) {
                result.append(", ");
            }
        }
        if (combination.size() > 1) {
            result.append(")");
        }

        // Done
        return result.toString();
    }
}
