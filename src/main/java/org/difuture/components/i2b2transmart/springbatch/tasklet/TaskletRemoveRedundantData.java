/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Visit;
import org.difuture.components.util.LoggerUtil;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Removes subjects and visits for which there is no data.
 * 
 * @author Claudia Lang
 */
public class TaskletRemoveRedundantData implements Tasklet {

    @Override
    public RepeatStatus execute(StepContribution contribution,
                                ChunkContext chunkContext) throws Exception {

        // Prepare
        LoggerUtil.info("Removing redundant data", this.getClass());
        int removedSubjects = 0;
        int removedVisits = 0;
        int removedVisitsWithoutData = 0;

        // Remove visits without data
        for (Subject subject : Model.get().getSubjects()) {

            // Remove all visits without subjects
            Iterator<Visit> visitIterator = subject.getVisits().iterator();
            while (visitIterator.hasNext()) {
                Visit visit = visitIterator.next();
                if (!visit.hasData()) {
                    visitIterator.remove();
                    Model.get().removeVisit(visit);
                    removedVisitsWithoutData++;
                }
            }
        }

        // Remove all subjects without visits
        Set<Visit> visitsToKeep = new HashSet<>();
        Iterator<Subject> subjectIterator = Model.get().getSubjects().iterator();
        while (subjectIterator.hasNext()) {
            Set<Visit> visits = subjectIterator.next().getVisits();
            if (visits.isEmpty() && !Model.get().getConfig().getSubjectConfiguration().keepAll) {
                subjectIterator.remove();
                removedSubjects++;
            } else {
                visitsToKeep.addAll(visits);
            }
        }

        // Remove all visits without subjects
        Iterator<Visit> visitIterator = Model.get().getVisits().iterator();
        while (visitIterator.hasNext()) {
            if (!visitsToKeep.contains(visitIterator.next())) {
                visitIterator.remove();
                removedVisits++;
            }
        }

        // Print status
        LoggerUtil.info(removedVisits,
                        " - Removed visits without data: " + removedVisitsWithoutData,
                        this.getClass());
        LoggerUtil.info(removedSubjects,
                        " - Removed non-joining subjects: " + removedSubjects,
                        this.getClass());
        LoggerUtil.info(removedVisits,
                        " - Removed non-joining visits: " + removedVisits,
                        this.getClass());

        // Introduce visit names (when only date has been defined)
        // TODO: Should be done elsewhere
        if (!Model.get().isI2b2()) {

            int created = 0;
            LoggerUtil.info("Generating visit names", this.getClass());
            for (Subject subject : Model.get().getSubjects()) {

                // Prepare
                List<Visit> visits = new ArrayList<>(subject.getVisits());
                Collections.sort(visits, new Comparator<Visit>() {
                    @Override
                    public int compare(Visit o1, Visit o2) {
                        if (o1.getStart() == null && o2.getStart() == null) { return 0; }
                        if (o1.getStart() == null && o2.getStart() != null) { return -1; }
                        if (o1.getStart() != null && o2.getStart() == null) { return +1; }
                        return o1.getStart().compareTo(o2.getStart());
                    }
                });

                // Replace with visit ID
                int id = 0;
                for (Visit visit : visits) {
                    if (visit.getName() == null) {
                        visit.setName("Visit-" + (id + 1));
                        created++;
                    }
                    id++;
                }
            }
            LoggerUtil.info(" - Names created: " + created, this.getClass());
        }

        // Done
        return RepeatStatus.FINISHED;
    }
}
