/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.springbatch.tasklet.TaskletCalculateTimestamps;
import org.difuture.components.i2b2transmart.springbatch.tasklet.TaskletCreateGenericEntities;
import org.difuture.components.i2b2transmart.springbatch.tasklet.TaskletGetConceptHierarchy;
import org.difuture.components.i2b2transmart.springbatch.tasklet.TaskletGetDataTypePerObservation;
import org.difuture.components.i2b2transmart.springbatch.tasklet.TaskletRemoveDuplicates;
import org.difuture.components.i2b2transmart.springbatch.tasklet.TaskletRemoveRedundantData;
import org.difuture.components.i2b2transmart.springbatch.tasklet.TaskletResetFolder;
import org.difuture.components.i2b2transmart.springbatch.tasklet.TaskletSelectVisits;
import org.difuture.components.i2b2transmart.springbatch.tasklet.read.ReadTaskletDataItem;
import org.difuture.components.i2b2transmart.springbatch.tasklet.read.ReadTaskletMappingItem;
import org.difuture.components.i2b2transmart.springbatch.tasklet.read.ReadTaskletSubjectsItem;
import org.difuture.components.i2b2transmart.springbatch.tasklet.read.ReadTaskletVisitsItem;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletBasicConfiguration;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletBasicConfigurationI2b2;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletBasicConfigurationTransmart;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletBasicConfigurationTransmartTMDL;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletDataFiles;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletDataFilesI2b2;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletDataFilesTransmart;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletDataFilesTransmartTMDL;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletMappingFile;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletMappingFileI2b2;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletMappingFileTransmart;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletMappingFileTransmartTMDL;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletOntology;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletSubjects;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletSubjectsI2b2;
import org.difuture.components.i2b2transmart.springbatch.tasklet.write.WriteTaskletVisitsI2b2;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Defines the Spring Batch Steps and the Spring Batch Job with the staging
 * functionality for i2b2 and for tranSMART.
 * 
 * @author Claudia Lang
 * 
 */
@Configuration
@EnableBatchProcessing
public class Staging {

    /** The job builder factory. Instantiated by spring batch framework. */
    @Autowired
    public JobBuilderFactory  jobBuilderFactory;

    /** The step builder factory. Instantiated by spring batch framework. */
    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    /**
     * Creates a new instance. Required by Spring Batch Framework.
     */
    public Staging() {
        // Empty by design
    }

    /**
     * Gets a tasklet to read the attributes of all input files.
     * 
     * @return a ReadTaskletAttributesItem
     */
    @Bean
    public ReadTaskletDataItem dataItemReadTasklet() {
        return new ReadTaskletDataItem();
    }

    /**
     * The staging job. Does the staging job for tranSMART or I2B2. Contains
     * several steps.
     *
     * @return a job
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Bean
    public Job doStagingJob() throws IOException {
        // TODO: Find a better solution
        if (Model.get().isI2b2()) {

            return this.jobBuilderFactory.get("doStagingJob")
                                         .start(stepResetFolder())
                                         .next(stepCreateGenericEntities())
                                         .next(stepGetSubjects())
                                         .next(stepGetVisits())
                                         .next(stepGetMappings())
                                         .next(stepGetData())
                                         .next(stepTimestampsAndCalculations())
                                         .next(stepRemoveRedundantData())
                                         .next(stepSelectVisits())
                                         .next(stepDataTypePerObservation())
                                         .next(stepDeDuplicate())
                                         .next(stepExtractConceptHierarchies())
                                         .next(stepWriteDataFiles())
                                         .next(stepWriteSubjects())
                                         .next(stepWriteVisits())
                                         .next(stepWriteMappingFile())
                                         .next(stepWriteBasicConfig())
                                         .next(stepWriteOntology())
                                         .build();
        } else {
            return this.jobBuilderFactory.get("doStagingJob")
                                         .start(stepResetFolder())
                                         .next(stepCreateGenericEntities())
                                         .next(stepGetSubjects())
                                         .next(stepGetVisits())
                                         .next(stepGetMappings())
                                         .next(stepGetData())
                                         .next(stepTimestampsAndCalculations())
                                         .next(stepRemoveRedundantData())
                                         .next(stepSelectVisits())
                                         .next(stepDataTypePerObservation())
                                         .next(stepDeDuplicate())
                                         .next(stepExtractConceptHierarchies())
                                         .next(stepWriteDataFiles())
                                         .next(stepWriteSubjects())
                                         .next(stepWriteMappingFile())
                                         .next(stepWriteBasicConfig())
                                         .build();
        }
    }

    /**
     * Gets time stamps for visits and assigns the oldest one to its subject
     * 
     * @return
     */
    @Bean
    public TaskletCalculateTimestamps getCalculateTimestampsTasklet() {
        return new TaskletCalculateTimestamps();
    }

    /**
     *
     * @return a TaskletGetConceptHierarchie
     */
    @Bean
    public TaskletGetConceptHierarchy getConceptHierarchieTasklet() {
        return new TaskletGetConceptHierarchy();
    }

    @Bean
    public TaskletCreateGenericEntities getCreateGenericEntitiesTasklet() {
        return new TaskletCreateGenericEntities();
    }

    /**
     * 
     * @return a GetDataTypePerObservationTasklet
     */
    @Bean
    public TaskletGetDataTypePerObservation getDataTypePerObservationTasklet() {
        return new TaskletGetDataTypePerObservation();
    }

    /**
     * Gets a tasklet which deletes duplicates.
     * 
     * @return an TaskletNonDuplicates
     */
    @Bean
    public TaskletRemoveDuplicates getNonDuplicatesTasklet() {
        return new TaskletRemoveDuplicates();
    }

    /**
     * Join visits with subjects. Used in a step to join Visit objects with
     * Subject objects.
     *
     * @return a JoinVisitsWithSubjectsTasklet
     */
    @Bean
    public TaskletRemoveRedundantData getRemoveRedundantDataTasklet() {
        return new TaskletRemoveRedundantData();
    }

    /**
     * Reset folder. Used in a step to reset folder. Is executed at the
     * beginning of the staging job to delete previous results.
     *
     * @return a ResetFolderTasklet
     */
    @Bean
    public TaskletResetFolder getResetFolderTasklet() {
        return new TaskletResetFolder();
    }

    @Bean
    public TaskletSelectVisits getSelectVisitsTasklet() {
        return new TaskletSelectVisits();
    }

    /**
     * Write basic configuration files for transmart.
     *
     * @return a WriteBasicConfigurationTransmartTasklet
     */
    @Bean
    public WriteTaskletBasicConfiguration getWriteBasicConfigTransmartTasklet() {
        Path pathName;
        WriteTaskletBasicConfiguration tasklet;
        if (Model.get().isUseTmDataLoader()) {
            String studyID = Model.get().getConfig().getStudyID();
            pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(),
                                 "tranSMART",
                                 studyID);
            tasklet = new WriteTaskletBasicConfigurationTransmartTMDL();
        } else {
            pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(),
                                 "tranSMART");
            tasklet = new WriteTaskletBasicConfigurationTransmart();
        }
        File folder = new File(pathName.toString());
        tasklet.setFolder(folder);
        return tasklet;
    }

    /**
     * Write data files for i2b2. Used in a step to write files (params files,
     * properties file, Visit file, Subject file, Attributes files).
     *
     * @return a WriteDataFilesTasklet
     */
    @Bean
    public WriteTaskletDataFilesI2b2 getWriteDataFilesi2b2Tasklet() {
        Path pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(), "i2b2");
        File folder = new File(pathName.toString());
        WriteTaskletDataFilesI2b2 tasklet = new WriteTaskletDataFilesI2b2();
        tasklet.setFolder(folder);
        return tasklet;
    }

    /**
     * Write data files for transmart. Used in a step to write files (params
     * files, properties file, Visit file, Subject file, Attributes files).
     *
     * @return a WriteDataFilesTasklet
     */
    @Bean
    public WriteTaskletDataFiles getWriteDataFilesTransmartTasklet() {
        Path pathName;
        WriteTaskletDataFiles tasklet;
        if (Model.get().isUseTmDataLoader()) {
            String studyID = Model.get().getConfig().getStudyID();
            pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(),
                                 "tranSMART",
                                 studyID,
                                 "Public Studies",
                                 studyID,
                                 "ClinicalDataToUpload");
            tasklet = new WriteTaskletDataFilesTransmartTMDL();
        } else {
            pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(),
                                 "tranSMART",
                                 "clinical");
            tasklet = new WriteTaskletDataFilesTransmart();
        }
        File folder = new File(pathName.toString());
        tasklet.setFolder(folder);
        return tasklet;
    }

    /**
     * Write mapping file for i2b2.
     *
     * @return a WriteMappingFileI2b2Tasklet
     */
    @Bean
    public WriteTaskletMappingFileI2b2 getWriteMappingFileI2b2Tasklet() {
        Path pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(), "i2b2");
        File folder = new File(pathName.toString());
        WriteTaskletMappingFileI2b2 tasklet = new WriteTaskletMappingFileI2b2();
        tasklet.setFolder(folder);
        tasklet.setFileName("column_map.tsv");
        return tasklet;
    }

    /**
     * Write mapping file for transmart.
     *
     * @return a WriteMappingFileTransmartTasklet
     */
    @Bean
    public WriteTaskletMappingFile getWriteMappingFileTransmartTasklet() {
        WriteTaskletMappingFile tasklet;
        if (Model.get().isUseTmDataLoader()) {
            String studyID = Model.get().getConfig().getStudyID();
            Path pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(),
                                      "tranSMART",
                                      studyID,
                                      "Public Studies",
                                      studyID,
                                      "ClinicalDataToUpload");
            File folder = new File(pathName.toString());
            tasklet = new WriteTaskletMappingFileTransmartTMDL();
            tasklet.setFolder(folder);
            tasklet.setFileName(studyID + "_Mapping_File.txt");
        } else {
            Path pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(),
                                      "tranSMART",
                                      "clinical");
            File folder = new File(pathName.toString());
            tasklet = new WriteTaskletMappingFileTransmart();
            tasklet.setFolder(folder);
            tasklet.setFileName("mapping.params");
        }
        return tasklet;
    }

    /**
     * Writes the ontology file for i2b2.
     * 
     *
     * @return a WriteOntologyTasklet
     */
    @Bean
    public WriteTaskletOntology getWriteOntologyTasklet() {
        Path pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(), "i2b2");
        File folder = new File(pathName.toString());
        WriteTaskletOntology tasklet = new WriteTaskletOntology();
        tasklet.setFolder(folder);
        return tasklet;
    }

    /**
     * Write subject objects to csv file for i2b2.
     *
     * @return a WriteSubjectsI2b2Tasklet
     */
    @Bean
    public WriteTaskletSubjectsI2b2 getWriteSubjectsI2b2Tasklet() {
        Path pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(), "i2b2");
        File folder = new File(pathName.toString());
        WriteTaskletSubjectsI2b2 tasklet = new WriteTaskletSubjectsI2b2();
        tasklet.setFolder(folder);
        return tasklet;
    }

    /**
     * Write subject objects to csv file for any DWH but i2b2 (so far this is
     * transmart).
     *
     * @return a WriteSubjectsTasklet
     */
    @Bean
    public WriteTaskletSubjects getWriteSubjectsTasklet() {
        Path pathName;

        if (Model.get().isUseTmDataLoader()) {
            String studyID = Model.get().getConfig().getStudyID();
            pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(),
                                 "tranSMART",
                                 studyID,
                                 "Public Studies",
                                 studyID,
                                 "ClinicalDataToUpload");
        } else {
            pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(),
                                 "tranSMART",
                                 "clinical");
        }
        File folder = new File(pathName.toString());
        WriteTaskletSubjects tasklet = new WriteTaskletSubjects();
        tasklet.setFolder(folder);
        return tasklet;
    }

    /**
     * Write visit objects to csv file for i2b2.
     *
     * @return a WriteVisitsI2b2Tasklet
     */
    @Bean
    public WriteTaskletVisitsI2b2 getWriteVisitsI2b2Tasklet() {
        Path pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(), "i2b2");
        File folder = new File(pathName.toString());
        WriteTaskletVisitsI2b2 tasklet = new WriteTaskletVisitsI2b2();
        tasklet.setFolder(folder);
        return tasklet;
    }

    /**
     * Gets a tasklet to read the mapping (visit to subject)
     * 
     * @return a ReadTaskletMappingItem
     */
    @Bean
    public ReadTaskletMappingItem mappingItemReadTasklet() {
        return new ReadTaskletMappingItem();
    }

    /**
     * Create generic entities
     * 
     * @return
     */
    public Step stepCreateGenericEntities() {
        return stepBuilderFactory.get("stepCreateGenericEntities")
                                 .tasklet(getCreateGenericEntitiesTasklet())
                                 .build();
    }

    /**
     * Step get data type per observation. Gets per configuration input file
     * their attributes and its data type
     * 
     * @return a step
     */
    @Bean
    public Step stepDataTypePerObservation() {
        return this.stepBuilderFactory.get("stepGetDataTypePerObservation")
                                      .tasklet(getDataTypePerObservationTasklet())
                                      .build();
    }

    /**
     * Step get non duplicates. Removes duplicates in attributes, if configured.
     *
     * @return a step
     */
    @Bean
    public Step stepDeDuplicate() {
        return this.stepBuilderFactory.get("stepGetNonDuplicates")
                                      .tasklet(getNonDuplicatesTasklet())
                                      .build();
    }

    /**
     * Step get observations.
     *
     * @return a step
     */
    @Bean
    public Step stepExtractConceptHierarchies() {
        return this.stepBuilderFactory.get("stepGetConceptHierarchie")
                                      .tasklet(getConceptHierarchieTasklet())
                                      .build();
    }

    /**
     * Step get attributes. Reads, processes and writes Attributes objects.
     *
     * @return a step
     */
    @Bean
    public Step stepGetData() {
        return this.stepBuilderFactory.get("stepGetData").tasklet(dataItemReadTasklet()).build();
    }

    /**
     * Step get mappings.
     *
     * @return a step
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Bean
    public Step stepGetMappings() throws IOException {
        return stepBuilderFactory.get("stepGetMappings").tasklet(mappingItemReadTasklet()).build();
    }

    /**
     * Step get subjects. Reads and processes Subject objects. *
     *
     * @return a step
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Bean
    public Step stepGetSubjects() throws IOException {
        return stepBuilderFactory.get("stepGetSubjects").tasklet(subjectItemReadTasklet()).build();
    }

    /**
     * Step get visits. Reads and processes Visit objects.
     *
     * @return a step
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Bean
    public Step stepGetVisits() throws IOException {
        return stepBuilderFactory.get("stepGetVisits").tasklet(visitItemReadTasklet()).build();
    }

    /**
     * Step join visits with subjects. Joins Visit objects with Subject objects.
     *
     * @return a step
     */
    @Bean
    public Step stepRemoveRedundantData() {
        return this.stepBuilderFactory.get("stepRemoveRedundantData")
                                      .tasklet(getRemoveRedundantDataTasklet())
                                      .build();
    }

    /**
     * Step reset folder.
     *
     * @return a step
     */
    @Bean
    public Step stepResetFolder() {
        return stepBuilderFactory.get("stepResetFolder").tasklet(getResetFolderTasklet()).build();
    }

    /**
     * Step select visits. Selects visits.
     * 
     * @return a step
     */
    @Bean
    public Step stepSelectVisits() {
        return this.stepBuilderFactory.get("stepSelectVisits")
                                      .tasklet(getSelectVisitsTasklet())
                                      .build();
    }

    /**
     * Step getting the time stamps of the visits and assigns the oldets one to
     * its subject. Calculates the age.
     * 
     * @return
     */
    @Bean
    public Step stepTimestampsAndCalculations() {
        return this.stepBuilderFactory.get("stepTimestampsAndCalculations")
                                      .tasklet(getCalculateTimestampsTasklet())
                                      .build();
    }

    /**
     * Step write basic configuration files.
     *
     * @return a step
     */
    @Bean
    public Step stepWriteBasicConfig() {
        if (Model.get()
                 .isI2b2()) { return this.stepBuilderFactory.get("stepWriteBasicConfig")
                                                            .tasklet(writeBasicConfigI2b2())
                                                            .build(); }
        return this.stepBuilderFactory.get("stepWriteBasicConfig")
                                      .tasklet(getWriteBasicConfigTransmartTasklet())
                                      .build();
    }

    /**
     * Step write data files. Writes files.
     *
     * @return a step
     */
    @Bean
    public Step stepWriteDataFiles() {
        if (Model.get().isI2b2()) {
            return this.stepBuilderFactory.get("stepWriteDataFiles")
                                          .tasklet(getWriteDataFilesi2b2Tasklet())
                                          .build();
        } else {
            return this.stepBuilderFactory.get("stepWriteDataFiles")
                                          .tasklet(getWriteDataFilesTransmartTasklet())
                                          .build();
        }
    }

    /**
     * Step write mapping file.
     *
     * @return a step
     */
    @Bean
    public Step stepWriteMappingFile() {
        if (Model.get()
                 .isI2b2()) { return this.stepBuilderFactory.get("stepWriteMappingFile")
                                                            .tasklet(getWriteMappingFileI2b2Tasklet())
                                                            .build(); }
        return this.stepBuilderFactory.get("stepWriteMappingFile")
                                      .tasklet(getWriteMappingFileTransmartTasklet())
                                      .build();
    }

    /**
     * Step write ontology files.
     *
     * @return a step
     */
    @Bean
    public Step stepWriteOntology() {
        return this.stepBuilderFactory.get("stepWriteOntology")
                                      .tasklet(getWriteOntologyTasklet())
                                      .build();
    }

    /**
     * Step write subject object to csv file.
     *
     * @return a step
     */
    @Bean
    public Step stepWriteSubjects() {
        if (Model.get()
                 .isI2b2()) { return this.stepBuilderFactory.get("stepWriteSubjects")
                                                            .tasklet(getWriteSubjectsI2b2Tasklet())
                                                            .build(); }
        return this.stepBuilderFactory.get("stepWriteSubjects")
                                      .tasklet(getWriteSubjectsTasklet())
                                      .build();
    }

    /**
     * Step write visit object to csv file.
     *
     * @return a step
     */
    @Bean
    public Step stepWriteVisits() {
        return this.stepBuilderFactory.get("stepWriteVisits")
                                      .tasklet(getWriteVisitsI2b2Tasklet())
                                      .build();
    }

    /**
     * Gets a tasklet to read the subjects.
     *
     * @return a ReadTaskletSubjectsItem
     */
    @Bean
    public ReadTaskletSubjectsItem subjectItemReadTasklet() {
        return new ReadTaskletSubjectsItem();
    }

    /**
     * Gets a tasklet to read the visits.
     *
     * @return a ReadTaskletVisitsItem
     */
    @Bean
    public ReadTaskletVisitsItem visitItemReadTasklet() {
        return new ReadTaskletVisitsItem();
    }

    /**
     * Write basic configuration files for i2b2.
     *
     * @return a WriteBasicConfigurationI2b2Tasklet
     */
    @Bean
    public WriteTaskletBasicConfigurationI2b2 writeBasicConfigI2b2() {
        Path pathName = Paths.get(Model.get().getConfig().getFolder().getAbsolutePath(), "i2b2");
        File folder = new File(pathName.toString());
        WriteTaskletBasicConfigurationI2b2 tasklet = new WriteTaskletBasicConfigurationI2b2();
        tasklet.setFolder(folder);
        return tasklet;
    }

}
