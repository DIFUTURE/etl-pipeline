/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.io.File;
import java.io.IOException;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

/**
 * Writes the basic configurations into files.
 * 
 * @author Claudia Lang
 *
 */
public abstract class WriteTaskletBasicConfiguration implements Tasklet, InitializingBean {

    /** Folder in which to write the files. */
    protected File folder;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(folder, "Folder for the basic configurations to write must not be null!");
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution,
                                ChunkContext chunkContext) throws Exception {
        writeBasicConfigurationFiles();
        return RepeatStatus.FINISHED;
    }

    /**
     * Sets the folder.
     *
     * TODO: why do we need this method? Why not set this.folder from within the
     * constructor? TODO: Does spring batch requir default constructor only?
     * 
     * @param folder
     *            the new folder
     */
    public void setFolder(File folder) {
        this.folder = folder;
    }

    /**
     * Write basic configuration files.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected abstract void writeBasicConfigurationFiles() throws IOException;

}
