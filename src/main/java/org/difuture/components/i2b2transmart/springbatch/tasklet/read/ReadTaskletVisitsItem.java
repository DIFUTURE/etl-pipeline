/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.read;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationEncounter;
import org.difuture.components.i2b2transmart.model.Key;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Timestamp;
import org.difuture.components.i2b2transmart.model.Visit;
import org.difuture.components.util.LoggerUtil;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * The Class VisitsItemReadTasklet. Gets the visits of the input file as
 * configured in configuration.properties. If there is no configuration for
 * encounter, nothing happens.
 * 
 * @author Claudia Lang
 */
public class ReadTaskletVisitsItem implements Tasklet {

    /** Counts the visits ignored due to incompatible date format. */
    private int ignoredDuToIncompatibleDateFormat;
    /** Contains the number of visits read. */
    private int numberOfVisits;
    /** The index of the column with the start date. */
    private int indexStart;
    /** The index of the column with the encounter id. */
    private int indexVisitID;

    // i2b2 specific
    /** The index of the column with he end date - i2b2 specific */
    private int indexEnd;
    /** The index of the column with the active status - i2b2 specific */
    private int indexActiveStatus;
    /** The index of the column with the in out - i2b2 specific */
    private int indexInOut;
    /** The index of the column with the location - i2b2 specific */
    private int indexLocation;
    /** The index of the column with the location path - i2b2 specific */
    private int indexLocationPath;
    /** The index of the column with the length of stay - i2b2 specific */
    private int indexLengthOfStay;
    /** The index of the column with the blob - i2b2 specific */
    private int indexBlob;

    @Override
    public RepeatStatus execute(StepContribution contribution,
                                ChunkContext chunkContext) throws Exception, IOException {

        LoggerUtil.info("Reading visit data", this.getClass());

        // Create artificial visit for each subject
        for (Subject subject : Model.get().getSubjects()) {
            Visit visit = Model.get().createVisit(subject);
            subject.getVisits().add(visit);
        }

        // Log
        LoggerUtil.info(" - Created " + Model.get().getNumSubjects() + " artificial visits",
                        this.getClass());

        // Break if no encounter configured
        if (Model.get().getConfig().getEncounterConfiguration() == null) {
            LoggerUtil.info("No encounter configured", this.getClass());
            return RepeatStatus.FINISHED;
        }

        // Prepare
        ConfigurationEncounter config = Model.get().getConfig().getEncounterConfiguration();
        ignoredDuToIncompatibleDateFormat = 0;
        numberOfVisits = 0;

        // Read csv file
        Set<String> columns = config.getSelectedColumns();
        List<String[]> rows = CSVReader.getRows(null, columns, config.file);

        // Parse header
        parseHeader(rows.get(0));

        // Read, skipping header
        for (int i = 1; i < rows.size(); i++) {
            createVisit(rows.get(i));
            numberOfVisits++;
        }

        // Print some info
        LoggerUtil.info(ignoredDuToIncompatibleDateFormat,
                        " - Ignored " + ignoredDuToIncompatibleDateFormat + " out of " +
                                                           numberOfVisits +
                                                           " visits due to incompatible date format",
                        this.getClass());
        LoggerUtil.info(" - Found " + Model.get().getNumVisits() + " visits", this.getClass());

        // Done
        return RepeatStatus.FINISHED;
    }

    /**
     * Creates a visit object basing on the row read and puts it to the map with
     * the visit.
     * 
     * @param visitString
     *            - a string array. Representing the row read with the visit
     *            data.
     */
    private void createVisit(String[] visitString) {

        // Prepare
        ConfigurationEncounter config = Model.get().getConfig().getEncounterConfiguration();
        List<String> values = Arrays.asList(visitString);
        Key encounter = config.key.getKey(values);
        Timestamp startTime = new Timestamp(Model.get()
                                                 .getConfig()
                                                 .getEncounterConfiguration()
                                                 .getDateFormatter(),
                                            values.get(indexStart),
                                            true);
        Timestamp endTime = new Timestamp(Model.get()
                                               .getConfig()
                                               .getEncounterConfiguration()
                                               .getDateFormatter(),
                                          values.get(indexEnd),
                                          false);

        if (!startTime.isValid()) {
            ignoredDuToIncompatibleDateFormat++;
            return;
        }
        if (!endTime.isValid()) {
            endTime = startTime;
        }

        // Create for i2b2
        if (Model.get().isI2b2()) {

            // Read data
            String activeStatus = indexActiveStatus != -1 ? values.get(indexActiveStatus) : null;
            String inout = indexInOut != -1 ? values.get(indexInOut) : null;
            String location = indexLocation != -1 ? values.get(indexLocation) : null;
            String locationPath = indexLocationPath != -1 ? values.get(indexLocationPath) : null;
            Integer lengthOfStay = indexLengthOfStay != -1
                    ? Integer.parseInt(values.get(indexLengthOfStay))
                    : null;
            String blob = indexBlob != -1 ? values.get(indexBlob) : null;

            // Create Visit for I2B2
            Model.get().createVisit(encounter,
                                    null,
                                    startTime,
                                    endTime,
                                    activeStatus,
                                    inout,
                                    location,
                                    locationPath,
                                    lengthOfStay,
                                    blob);

        } else {

            // Create visit for tranSMART
            Model.get().createVisit(encounter,
                                    config.visitID == null ? null : values.get(indexVisitID),
                                    startTime,
                                    endTime);
        }
    }

    /**
     * The first line read contains the header. The header is used to get the
     * indexes of the columns in which the data for the visits are.
     * 
     * @param line
     *            - a string array
     * @throws IOException
     */
    private void parseHeader(String[] line) throws IOException {

        // Prepare
        ConfigurationEncounter config = Model.get().getConfig().getEncounterConfiguration();
        List<String> headers = Arrays.asList(line);

        // Extract indexes
        indexVisitID = headers.indexOf(config.visitID);
        indexStart = headers.indexOf(config.start);
        indexEnd = headers.indexOf(config.end);

        if (Model.get().isI2b2()) {
            indexActiveStatus = headers.indexOf(config.activeStatus);
            indexInOut = headers.indexOf(config.inout);
            indexLocation = headers.indexOf(config.location);
            indexLocationPath = headers.indexOf(config.locationPath);
            indexLengthOfStay = headers.indexOf(config.lengthOfStay);
            indexBlob = headers.indexOf(config.blob);

            if (indexStart == -1) {
                LoggerUtil.fatalUnchecked("Admission start date not found: " + config.start,
                                          this.getClass(),
                                          IllegalArgumentException.class);
            }
            ;
            if (indexEnd == -1) {
                LoggerUtil.fatalUnchecked("Admission end date not found: " + config.end,
                                          this.getClass(),
                                          IllegalArgumentException.class);
            }
            ;
        }

        /*
         * Add indexes and field names needed. It is important that the indexes
         * and their corresponding field name are sorted ascending, for Spring
         * Batch assigns the content of the first column read to the field name
         * mentioned first and so on
         */
        try {
            config.key.calculateIndexes(headers);
        } catch (Exception e) {
            LoggerUtil.fatalUnchecked("File in question: " + config.file.getName(),
                                      e,
                                      this.getClass(),
                                      IllegalStateException.class);
        }

        // Extract date format
        if (Model.get().getConfig().getEncounterConfiguration().getDateFormatter() == null) {
            LoggerUtil.fatalUnchecked("No date format specified. TODO: implement",
                                      this.getClass(),
                                      IllegalArgumentException.class);
        }

        LoggerUtil.info(" - Interpreting " + config.start + " as a date with format " +
                        Model.get().getConfig().getEncounterConfiguration().getDateFormatter(),
                        this.getClass());
        LoggerUtil.info(" - Interpreting " + config.end + " as a date with format " +
                        Model.get().getConfig().getEncounterConfiguration().getDateFormatter(),
                        this.getClass());
    }
}
