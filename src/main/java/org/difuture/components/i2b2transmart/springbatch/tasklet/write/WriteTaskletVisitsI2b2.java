/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.difuture.components.i2b2transmart.configuration.ConfigurationEncounter;
import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Visit;

/**
 * Writes visit-data.
 * 
 * @author Claudia Lang
 *
 */
public class WriteTaskletVisitsI2b2 extends WriteTaskletVisits {

    /**
     * Writes the visits
     * 
     * @throws IOException
     */
    protected void writeVisits() throws IOException {
        // Prepare
        String delimiter = ConfigurationFileFormat.getInstance().getDelimiterOutput();
        ConfigurationEncounter config = Model.get().getConfig().getEncounterConfiguration();
        StringBuilder row = new StringBuilder();

        // Build header
        row.append("__SUBJECT_ID__");
        row.append(delimiter);
        row.append("__VISIT_ID__");
        // Start date (Mandatory)
        row.append(delimiter);
        row.append(config != null ? config.start : "__START__");
        // End date (Optional)
        // If no encounter was configured, artificial visits are used and they
        // have an end date
        if (config == null || config.end != null) {
            row.append(delimiter);
            row.append(config != null ? config.end : "__END__");
        }

        // If the property Encounter is configured in configuration.properties
        if (config != null) {
            if (config.activeStatus != null) {
                row.append(delimiter);
                row.append(config.activeStatus);
            }
            if (config.inout != null) {
                row.append(delimiter);
                row.append(config.inout);
            }
            if (config.location != null) {
                row.append(delimiter);
                row.append(config.location);
            }
            if (config.locationPath != null) {
                row.append(delimiter);
                row.append(config.locationPath);
            }
            if (config.lengthOfStay != null) {
                row.append(delimiter);
                row.append(config.lengthOfStay);
            }
            if (config.blob != null) {
                row.append(delimiter);
                row.append(config.blob);
            }
        }

        String newLine = ConfigurationFileFormat.getInstance().getNewLine();

        row.append(newLine);

        // Prepare writer
        File outputVisit = new File(folder.getAbsolutePath() + "/___visits___.csv");
        Writer writer = new FileWriter(outputVisit);

        // Write header
        writer.write(row.toString());

        // For each visit
        for (Visit visit : Model.get().getVisits()) {

            // Write
            row = new StringBuilder();
            row.append(visit.getSubject().getId().toString());
            row.append(delimiter);
            row.append(visit.getId().toString());
            row.append(delimiter);
            row.append(visit.getStart().format(ConfigurationFileFormat.getInstance()
                                                                      .getOutputDateFormatter()));
            // Start date is mandatory but end date is optional
            if (visit.getEnd() != null) {
                row.append(delimiter);
                row.append(visit.getEnd().format(ConfigurationFileFormat.getInstance()
                                                                        .getOutputDateFormatter()));
            }
            row.append(newLine);
            writer.write(row.toString());
        }
        writer.close();
    }
}
