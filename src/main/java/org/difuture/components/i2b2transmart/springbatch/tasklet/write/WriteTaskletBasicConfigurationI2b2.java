/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.difuture.components.i2b2transmart.configuration.Configuration;
import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.util.LoggerUtil;

/**
 * Writes the basic configurations for I2b2 into files.
 * 
 * @author Claudia Lang
 *
 */
public class WriteTaskletBasicConfigurationI2b2 extends WriteTaskletBasicConfiguration {

    /**
     * Write basic configuration files for i2b2 (.params, .properties).
     *
     * @param folder
     *            the folder
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    protected void writeBasicConfigurationFiles() throws IOException {

        LoggerUtil.info("Writing basic configuration files", this.getClass());

        Configuration config = Model.get().getConfig();
        String newLine = ConfigurationFileFormat.getInstance().getNewLine();

        // Backout
        File file = new File(folder.getAbsolutePath() + "/backout.params");
        Writer writer = new FileWriter(file);
        writer.close();

        // i2b2
        file = new File(folder.getAbsolutePath() + "/i2b2.params");
        writer = new FileWriter(file);
        writer.write("COLUMN_MAP_FILE=column_map.tsv" + newLine);
        writer.write("SOURCE_SYSTEM=" + Model.get().getConfig().getSourceSystemCode() + newLine);
        writer.write("DATE_FORMAT=" + ConfigurationFileFormat.getInstance().getOutputDateFormat() +
                     newLine);
        writer.close();

        // Database parameters
        file = new File(folder.getAbsolutePath() + "/batchdb.properties");
        writer = new FileWriter(file);
        writer.write("batch.jdbc.driver=org.postgresql.Driver");
        writer.write(newLine);
        writer.write("batch.jdbc.url=");
        writer.write(config.getDatabaseConfiguration().getUrl());
        writer.write(newLine);
        writer.write("batch.jdbc.user=");
        writer.write(config.getDatabaseConfiguration().getUser());
        writer.write(newLine);
        writer.write("batch.jdbc.password=");
        writer.write(config.getDatabaseConfiguration().getPassword());
        writer.close();
    }

}
