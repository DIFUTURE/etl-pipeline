/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.difuture.components.i2b2transmart.configuration.Configuration;
import org.difuture.components.i2b2transmart.configuration.ConfigurationDatabase;
import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.util.LoggerUtil;

/**
 * Writes the basic configurations for TranSMART into files, using loading tool
 * component-tm-dataloader
 * 
 * @author Claudia Lang
 *
 */
public class WriteTaskletBasicConfigurationTransmartTMDL extends WriteTaskletBasicConfiguration {

    /**
     * Write basic configuration files for TranSMART (.params, .properties).
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected void writeBasicConfigurationFiles() throws IOException {

        LoggerUtil.info("Writing basic configuration files", this.getClass());
        Configuration config = Model.get().getConfig();
        String newLine = ConfigurationFileFormat.getInstance().getNewLine();

        // Config.groovy
        File file = new File(folder.getAbsolutePath() + "/Config.groovy");
        Writer writer = new FileWriter(file);

        // Database parameters
        ConfigurationDatabase configDB = config.getDatabaseConfiguration();

        // Hostname
        writer.write("db.hostname = '" + configDB.getHost() + "'");
        writer.write(newLine);

        // Port
        writer.write("db.port = " + configDB.getPort());
        writer.write(newLine);

        // User name - tm_dataloader is mandatory
        writer.write("db.username = 'tm_dataloader'");
        writer.write(newLine);

        // Password
        writer.write("db.password = 'tm_dataloader'");
        writer.write(newLine);

        // Conncetion
        writer.write("db.jdbcConnectionString = \"jdbc:postgresql://${db.hostname}:${db.port}/transmart\"");
        writer.write(newLine);
        writer.write("db.jdbcDriver = 'org.postgresql.Driver'");
        writer.write(newLine);
        writer.write("db.sql.storedProcedureSyntax = 'PostgreSQL'");
        writer.write(newLine);

        // Data
        writer.write("dataDir = '" + folder.getAbsolutePath().replace("\\", "/") + "'");
        writer.write(newLine);

        // Properties
        if (Model.get().getConfig().getVisitTop()) {
            writer.write("visitNameFirst = true");
            writer.write(newLine);
            writer.write("alwaysSetVisitName = true");
            writer.write(newLine);
        }
        // TODO: Support other properties
        writer.write("no-rename");
        writer.write(newLine);

        writer.close();

    }
}
