/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.difuture.components.i2b2transmart.configuration.Configuration;
import org.difuture.components.i2b2transmart.configuration.ConfigurationDatabase;
import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.util.LoggerUtil;

/**
 * Writes the basic configurations for TranSMART into files.
 * 
 * @author Claudia Lang
 *
 */
public class WriteTaskletBasicConfigurationTransmart extends WriteTaskletBasicConfiguration {

    /**
     * Write basic configuration files for TranSMART (.params, .properties),
     * using loading tool component-transmart-batch.
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected void writeBasicConfigurationFiles() throws IOException {

        LoggerUtil.info("Writing basic configuration files", this.getClass());

        Configuration config = Model.get().getConfig();
        String newLine = ConfigurationFileFormat.getInstance().getNewLine();

        // Backout
        File file = new File(folder.getAbsolutePath() + "/backout.params");
        Writer writer = new FileWriter(file);
        writer.close();

        // Study
        file = new File(folder.getAbsolutePath() + "/study.params");
        writer = new FileWriter(file);
        writer.write("STUDY_ID=");
        writer.write(config.getStudyID());
        writer.write(newLine);
        writer.write("SECURITY_REQUIRED=");
        writer.write(config.isSecurityRequired() ? "Y" : "N");
        writer.write(newLine);
        writer.close();

        // Clinical
        file = new File(folder.getAbsolutePath() + "/clinical.params");
        writer = new FileWriter(file);
        writer.write("COLUMN_MAP_FILE=mapping.params" + newLine);
        writer.close();

        // Database parameters
        ConfigurationDatabase configDB = config.getDatabaseConfiguration();
        file = new File(folder.getAbsolutePath() + "/batchdb.properties");
        writer = new FileWriter(file);
        writer.write("batch.jdbc.driver=org.postgresql.Driver");
        writer.write(newLine);
        writer.write("batch.jdbc.url=");
        writer.write(configDB.getUrl());
        writer.write(newLine);
        writer.write("batch.jdbc.user=");
        writer.write(configDB.getUser());
        writer.write(newLine);
        writer.write("batch.jdbc.password=");
        writer.write(configDB.getPassword());
        writer.close();
    }

}
