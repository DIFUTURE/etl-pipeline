/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.read;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationMapping;
import org.difuture.components.i2b2transmart.model.Key;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Visit;
import org.difuture.components.util.LoggerUtil;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Maps each encounter to its corresponding subject, if available.
 * 
 * @author Claudia Lang
 * @author Fabian Prasser
 */
public class ReadTaskletMappingItem implements Tasklet {

    @Override
    public RepeatStatus execute(StepContribution contribution,
                                ChunkContext chunkContext) throws Exception {

        // If an encounter has been configured, load the mapping.
        if (Model.get()
                 .getConfig()
                 .getEncounterConfiguration() == null) { return RepeatStatus.FINISHED; }

        LoggerUtil.info("Reading mappings", this.getClass());

        // Prepare
        ConfigurationMapping config = Model.get().getConfig().getMappingConfiguration();
        Set<String> columns = config.getSelectedColumns();
        List<String[]> rows = CSVReader.getRows(null, columns, config.file);
        List<String> header = Arrays.asList(rows.get(0));
        int mappings = 0;

        // Initialize keys
        try {
            config.keyEncounter.calculateIndexes(header);
        } catch (Exception e) {
            LoggerUtil.fatalUnchecked("File in question: " + config.file.getName(),
                                      e,
                                      this.getClass(),
                                      IllegalStateException.class);
        }
        try {
            config.keySubject.calculateIndexes(header);
        } catch (Exception e) {
            LoggerUtil.fatalUnchecked("File in question: " + config.file.getName(),
                                      e,
                                      this.getClass(),
                                      IllegalStateException.class);
        }
        // For each row (skip header)
        for (int i = 1; i < rows.size(); i++) {

            // Obtain keys
            List<String> cells = Arrays.asList(rows.get(i));
            Key encounterID = config.keyEncounter.getKey(cells);
            Key subjectID = config.keySubject.getKey(cells);

            // Obtain objects
            Subject subject = Model.get().getSubject(subjectID);
            Visit visit = Model.get().getVisit(encounterID);

            // Associate
            if (subject != null && visit != null) {
                visit.setSubject(Model.get().getSubject(subjectID));
                subject.getVisits().add(visit);
                mappings++;
            }
        }

        // Log
        LoggerUtil.info("Found " + mappings + " mappings", this.getClass());

        // Done
        return RepeatStatus.FINISHED;
    }
}
