/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import org.difuture.components.i2b2transmart.configuration.ConfigurationSubject;
import org.difuture.components.i2b2transmart.model.OntologyNodeConcept;
import org.difuture.components.i2b2transmart.model.OntologyNodeModifier;
import org.difuture.components.util.LoggerUtil;

/**
 * Statements for i2b2 ontology tables
 * 
 * @author Helmut Spengler
 * @author Claudia Lang
 */
public class I2b2OperationsOntology {

    /** Insert statement for table i2b2demodata.concept_dimension */
    private final PreparedStatement insertConceptDimStmt;

    /** Insert statement for table i2b2demodata.modifier_dimension */
    private final PreparedStatement insertModDimStmt;

    /** Insert statement for table i2b2demodata.qt_breakdown_path */
    private final PreparedStatement insertBreakPathStmt;

    /** Insert statement for table i2b2metadata.custom_meta */
    private final PreparedStatement insertCustomMetaStmt;
    private boolean                 insertLevel0 = true;  // TODO:

    /** Insert statement for table i2b2metadata.i2b2 */
    private final PreparedStatement insertI2b2Stmt;

    /** Insert statement for table i2b2metadata.table_access */
    private final PreparedStatement insertTableAccessStmt;

    /** Insert statement for table i2b2demodata.code_lookup */
    private final PreparedStatement insertCodeLookupStmt;

    /** Time at creation of instance */
    private final Timestamp         nowTs;

    /**
     * Name of the table. This attribute appears in the i2b2 table TABLE_ACCESS
     */
    private String                  c_table_cd;

    /**
     * Name of the table. This attribute appears in the i2b2 table TABLE_ACCESS
     */
    private String                  c_table_name;

    /**
     * Creates a new instance
     * 
     * @param con
     * @throws SQLException
     */
    public I2b2OperationsOntology(Connection con,
                                  ConfigurationSubject configSubject) throws SQLException {
        nowTs = Timestamp.valueOf(LocalDateTime.now());
        c_table_cd = "i2b2";

        /*
         * Possible values are
         * 
         * CUSTOM_META if c_hlevel = 0 I2B2 if c_hlevel = 1
         * 
         * user defined
         * 
         */
        c_table_name = "I2B2";

        insertCustomMetaStmt = con.prepareStatement("INSERT INTO i2b2metadata.custom_meta (" +
                                                    "     c_hlevel,       c_fullname,         c_name," +
                                                    "     c_synonym_cd,   c_visualattributes, c_facttablecolumn," +
                                                    "     c_tablename,    c_columnname,       c_columndatatype," +
                                                    "     c_operator,     c_dimcode,          c_tooltip," +
                                                    "     m_applied_path, update_date,        sourcesystem_cd," +
                                                    "     c_basecode,     c_metadataxml" +
                                                    ") VALUES (" +
                                                    "     ?,              ?,                  ?," +
                                                    "     ?,              ?,                  ?," +
                                                    "     ?,              ?,                  ?," +
                                                    "     ?,              ?,                  ?," +
                                                    "     ?,              ?,                  ?," +
                                                    "     ?,              ?" + ")");

        insertI2b2Stmt = con.prepareStatement("INSERT INTO i2b2metadata.i2b2 (" +
                                              "     c_hlevel,       c_fullname,         c_name," +
                                              "     c_synonym_cd,   c_visualattributes, c_facttablecolumn," +
                                              "     c_tablename,    c_columnname,       c_columndatatype," +
                                              "     c_operator,     c_dimcode,          c_tooltip," +
                                              "     m_applied_path, update_date,        sourcesystem_cd," +
                                              "     c_basecode,     c_metadataxml" + ") VALUES (" +
                                              "     ?,              ?,                  ?," +
                                              "     ?,              ?,                  ?," +
                                              "     ?,              ?,                  ?," +
                                              "     ?,              ?,                  ?," +
                                              "     ?,              ?,                  ?," +
                                              "     ?,              ?" + ")");

        insertConceptDimStmt = con.prepareStatement("INSERT INTO i2b2demodata.concept_dimension (" +
                                                    "     concept_cd,     concept_path,   name_char," +
                                                    "     update_date,    download_date," +
                                                    "     import_date,    sourcesystem_cd" +
                                                    ") VALUES (" +
                                                    "     ?,                  ?,              ?," +
                                                    "     ?,                  ?," +
                                                    "     ?,                  ?" + ")");

        insertModDimStmt = con.prepareStatement("INSERT INTO i2b2demodata.modifier_dimension (" +
                                                "     modifier_cd,     modifier_path,   name_char," +
                                                "     update_date,    download_date," +
                                                "     import_date,    sourcesystem_cd" +
                                                ") VALUES (" +
                                                "     ?,                  ?,              ?," +
                                                "     ?,                  ?," +
                                                "     ?,                  ?" + ")");

        insertBreakPathStmt = con.prepareStatement("INSERT INTO i2b2demodata.qt_breakdown_path (" +
                                                   "	  name, 		value" + ") VALUES (" +
                                                   "		?,				?" + ")");

        insertCodeLookupStmt = con.prepareStatement("INSERT INTO i2b2demodata.code_lookup (" +
                                                    "	table_cd,		column_cd,		code_cd,	name_char" +
                                                    ") VALUES (" +
                                                    "		?,				?,				?,			?" +
                                                    ")");

        insertTableAccessStmt = con.prepareStatement("INSERT INTO i2b2metadata.table_access (" +
                                                     "     c_table_cd,        c_table_name,        c_protected_access, c_hlevel," +
                                                     "     c_fullname,        c_name,              c_synonym_cd,       c_visualattributes," +
                                                     "     c_facttablecolumn, c_dimtablename,      c_columnname,       c_columndatatype," +
                                                     "     c_operator,        c_dimcode,           c_tooltip" +
                                                     ") VALUES (" +
                                                     "  			?,             ?,      'N',                 1," +
                                                     "     ?,                  ?,                  'N',                 '" +
                                                     "FA" + "'," +
                                                     "     'concept_cd',      'concept_dimension', 'concept_path',     'T'," +
                                                     "     'LIKE',             ?,                   ?" +
                                                     ")");
    }

    /**
     * Adds a tree to the forest
     * 
     * @param File
     *            with serialized ontology
     * @param sourceSystem
     *            source system
     * @throws SQLException
     * @throws IOException
     */
    public void insert(File file, String sourceSystem) throws SQLException, IOException {

        OntologyNodeConcept root = OntologyNodeConcept.deserialize(file);
        int size = root.getSize();
        int sizeModifiers = 0;
        for (OntologyNodeConcept node : root.getChildren()) {
            if (node instanceof OntologyNodeModifier) {
                sizeModifiers++;
            }
        }

        LoggerUtil.info(" - Inserting ontologies for entity '" + root.getName() + "' with " +
                        (size - sizeModifiers) + " concepts and " + sizeModifiers + " modifiers",
                        this.getClass());

        // Init stack
        Stack<OntologyNodeConcept> stack = new Stack<>();

        // Push the root node to the stack
        stack.push(root);

        // Work until stack is empty
        int done = 0;
        int percOld = -1;
        while (!stack.isEmpty()) {

            // Insert into database
            OntologyNodeConcept node = stack.pop();

            // Nodes tho hide won't be inserted
            if (!node.getHide()) {
                insertNode(node, sourceSystem);
            }
            done++;
            int perc = (int) Math.round((double) done / (double) size * 100d);
            if (perc % 10 == 0 && perc != percOld) {
                LoggerUtil.info("   * Progress: " + perc + "%", this.getClass());
                percOld = perc;
            }

            // Handle children
            for (OntologyNodeConcept child : node.getChildren()) {
                stack.push(child);
            }
        }
    }

    public void insertCodeLookup() throws SQLException {
        String tableCd = "PATIENT_DIMENSION";

        // Insert LANGUAGE_CD
        insertCodeLookup(tableCd, "LANGUAGE_CD", I2b2TableContents.CODE_LOOKUP_PAT_LANG);

        // Insert VITAL_STATUS_CD
        insertCodeLookup(tableCd, "VITAL_STATUS_CD", I2b2TableContents.CODE_LOOKUP_PAT_VIT);

        // Insert RACE_CD
        insertCodeLookup(tableCd, "RACE_CD", I2b2TableContents.CODE_LOOKUP_PAT_RACE);

        // Insert RELIGION_CD
        insertCodeLookup(tableCd, "RELIGION_CD", I2b2TableContents.CODE_LOOKUP_PAT_REL);

        // Insert SEX_CD
        insertCodeLookup(tableCd, "SEX_CD", I2b2TableContents.CODE_LOOKUP_PAT_SEX);
    }

    private void insertCodeLookup(String tableCd,
                                  String columnCd,
                                  Map<String, String> data) throws SQLException {

        for (Entry<String, String> entry : data.entrySet()) {
            insertCodeLookupStmt.setString(1, tableCd); // table_cd // 1
            insertCodeLookupStmt.setString(2, columnCd); // column_cd // 2
            insertCodeLookupStmt.setString(3, entry.getKey()); // code_cd // 3
            insertCodeLookupStmt.setString(4, entry.getValue()); // name_char //
                                                                 // 4
            insertCodeLookupStmt.execute();
            insertCodeLookupStmt.clearParameters();
        }

    }

    /**
     * Insert a single node into the relevant DB tables. Does NOT check, if
     * ancestors in ontology exist.
     * 
     * @param node
     * @param sourceSystem
     * @throws SQLException
     */
    private void insertNode(OntologyNodeConcept node, String sourceSystem) throws SQLException {
        // The root node needs to also be referenced in a separate table
        if (insertLevel0) {
            insertLevel0 = false;
            // c_hlevel, c_fullname, c_name," // 1, 2, 3
            // c_synonym_cd, c_visualattributes, c_facttablecolumn," // 4, 5, 6
            // c_tablename, c_columnname, c_columndatatype," // 7, 8, 9
            // c_operator, c_dimcode, c_tooltip," // 10, 11, 12
            // m_applied_path, update_date, sourcesystem_cd" // 13, 14, 15
            // c_basecode, c_metadataxml" // 16, 17
            insertCustomMetaStmt.setInt(1, 0);
            insertCustomMetaStmt.setString(2, "\\Custom Metadata\\");
            insertCustomMetaStmt.setString(3, "Custom Metadata");
            insertCustomMetaStmt.setString(4, "N");
            insertCustomMetaStmt.setString(5, "CAE");
            insertCustomMetaStmt.setString(6, "concept_cd");
            insertCustomMetaStmt.setString(7, "concept_dimension");
            insertCustomMetaStmt.setString(8, "concept_path");
            insertCustomMetaStmt.setString(9, "T");
            insertCustomMetaStmt.setString(10, "LIKE");
            insertCustomMetaStmt.setString(11, "\\Custom Metadata\\");
            insertCustomMetaStmt.setString(12, "Custom Metadata");
            insertCustomMetaStmt.setString(13, "@");
            insertCustomMetaStmt.setTimestamp(14, nowTs);
            insertCustomMetaStmt.setString(15, sourceSystem);
            insertCustomMetaStmt.setString(16, "");
            insertCustomMetaStmt.setString(17, "");

            insertCustomMetaStmt.execute();
        }

        // Concept nodes in level 1 need to also be referenced in a separate
        // table

        if (node.getDepth() == 1) {

            // c_table_cd, c_table_name, c_protected_access, c_hlevel," // 1, 2,
            // 3, 4
            // c_fullname, c_name, c_synonym_cd, c_visualattributes," // 5, 6,
            // 7, 8
            // c_facttablecolumn, c_dimtablename, c_columnname,
            // c_columndatatype," // 9, 10, 11, 12
            // c_operator, c_dimcode, c_tooltip" // 13, 14, 15
            // VALUES ("
            // 'CUST', 'CUSTOM_META', 'N', 0," // 1, 2, 3, 4
            // ?, ?, 'N', '" + visAttrRoot +"'," // 5, 6, 7, 8
            // 'concept_cd', 'concept_dimension', 'concept_path', 'T'," // 9,
            // 10, 11, 12
            // 'LIKE', ?, ?" // 13, 14, 15
            /*
             * insertTableAccessStmt.setString(1, c_table_cd);
             * insertTableAccessStmt.setString(2, node.getPath());
             * insertTableAccessStmt.setString(3, node.getName());
             * insertTableAccessStmt.setString(4, node.getPath());
             * insertTableAccessStmt.setString(5, node.getName());
             */
            insertTableAccessStmt.setString(1, c_table_cd);
            insertTableAccessStmt.setString(2, c_table_name);
            insertTableAccessStmt.setString(3, node.getPath());
            insertTableAccessStmt.setString(4, node.getName());
            insertTableAccessStmt.setString(5, node.getPath());
            insertTableAccessStmt.setString(6, node.getName());

            insertTableAccessStmt.execute();

        }

        // c_hlevel, c_fullname, c_name," // 1, 2, 3
        // c_synonym_cd, c_visualattributes, c_facttablecolumn," // 4, 5, 6
        // c_tablename, c_columnname, c_columndatatype," // 7, 8, 9
        // c_operator, c_dimcode, c_tooltip," // 10, 11, 12
        // m_applied_path, update_date, sourcesystem_cd" // 13, 14, 15
        // c_basecode, c_metadataxml" // 16, 17
        insertI2b2Stmt.setString(2, node.getFullName());
        insertI2b2Stmt.setString(3, node.getName());
        insertI2b2Stmt.setString(4, "N");
        insertI2b2Stmt.setString(5, node.getVisualAttributes());
        if (node instanceof OntologyNodeModifier) {
            // NOTE: Modifiers can have a hierarchy (beginning with 1, 2, 3...)
            // just like the concepts, but right now
            // we use them flat.
            int level = node.isLeaf() ? 2 : 1;
            if (node.getDataType().equals("N")) {
                level = 1;
            }
            // level = 1;
            insertI2b2Stmt.setInt(1, level);
            insertI2b2Stmt.setString(6, "modifier_cd");
            insertI2b2Stmt.setString(7, "modifier_dimension");
            insertI2b2Stmt.setString(8, "modifier_path");
        } else {
            insertI2b2Stmt.setInt(1, node.getDepth());
            insertI2b2Stmt.setString(6, "concept_cd");
            insertI2b2Stmt.setString(7, "concept_dimension");
            insertI2b2Stmt.setString(8, "concept_path");
        }
        // NOTE: doesn't work with 'N', so even for numeric values 'T' is used.
        // Else numeric queries will in i2b2 result in an error message
        // (Error.Error)
        /*
         * if (node.getDataType().equals("N")) { insertI2b2Stmt.setString(9,
         * node.getDataType()); insertI2b2Stmt.setString(10, "="); } else {
         * insertI2b2Stmt.setString(9, node.getDataType());
         * insertI2b2Stmt.setString(10, "LIKE"); }
         */
        insertI2b2Stmt.setString(9, "T");// <- doesnt work with numerics
        insertI2b2Stmt.setString(10, "LIKE");
        insertI2b2Stmt.setString(11, node.getPath());
        insertI2b2Stmt.setString(12, node.getPath());
        if (node instanceof OntologyNodeModifier) {
            // NOTE: The value for this attribute (m_applied_path) maps the
            // modifier to its concept(s)
            // If the path ends with "%" the modifier is mapped to all
            // subconcepts automatically
            // insertI2b2Stmt.setString(13, ((NodeModifier)node).getTargetPath()
            // + "%");
            insertI2b2Stmt.setString(13, ((OntologyNodeModifier) node).getTargetPath());
        } else {
            insertI2b2Stmt.setString(13, "@");
        }
        insertI2b2Stmt.setTimestamp(14, nowTs);
        insertI2b2Stmt.setString(15, sourceSystem);
        insertI2b2Stmt.setString(16, node.isLeaf() ? node.getCode() : "");
        insertI2b2Stmt.setString(17, node.getXML());
        insertI2b2Stmt.execute();

        if (node instanceof OntologyNodeModifier) {
            // modifier_cd, modifier_path, name_char // 1, 2, 3
            // update_date, download_date, // 4, 5
            // import_date, sourcesystem_cd // 6, 7
            insertModDimStmt.setString(1, node.getCode());
            insertModDimStmt.setString(2, node.getPath());
            insertModDimStmt.setString(3,
                                       node.isLeaf() ? (node.getName() + ":" + node.getCode())
                                               : node.getName());
            insertModDimStmt.setTimestamp(4, nowTs);
            insertModDimStmt.setTimestamp(5, nowTs);
            insertModDimStmt.setTimestamp(6, nowTs);
            insertModDimStmt.setString(7, sourceSystem);
            insertModDimStmt.execute();

        } else {
            // concept_cd, concept_path, name_char // 1, 2, 3
            // update_date, download_date, // 4, 5
            // import_date, sourcesystem_cd // 6, 7
            insertConceptDimStmt.setString(1, node.getCode());
            insertConceptDimStmt.setString(2, node.getPath());
            insertConceptDimStmt.setString(3,
                                           node.isLeaf() ? (node.getName() + ":" + node.getCode())
                                                   : node.getName());
            insertConceptDimStmt.setTimestamp(4, nowTs);
            insertConceptDimStmt.setTimestamp(5, nowTs);
            insertConceptDimStmt.setTimestamp(6, nowTs);
            insertConceptDimStmt.setString(7, sourceSystem);
            insertConceptDimStmt.execute();
        }

        // TODO: change -> createDataFiles
        /*
         * if (node.getName().equals(configSubject.sex)) {
         * insertBreakPathStmt.setString(1, "PATIENT_GENDER_COUNT_XML");
         * insertBreakPathStmt.setString(2, "\\\\" + nameCTableCd +
         * node.getPath()); insertBreakPathStmt.execute(); } else if
         * (node.getName().equals(configSubject.age)) {
         * insertBreakPathStmt.setString(1, "PATIENT_AGE_COUNT_XML");
         * insertBreakPathStmt.setString(2, "\\\\" + nameCTableCd +
         * node.getPath()); insertBreakPathStmt.execute(); } else if
         * (node.getName().equals(configSubject.race)) {
         * insertBreakPathStmt.setString(1, "PATIENT_RACE_COUNT_XML");
         * insertBreakPathStmt.setString(2, "\\\\" + nameCTableCd +
         * node.getPath()); insertBreakPathStmt.execute(); } else if
         * (node.getName().equals(configSubject.vitalStatus)) {
         * insertBreakPathStmt.setString(1, "PATIENT_VITALSTATUS_COUNT_XML");
         * insertBreakPathStmt.setString(2, "\\\\" + nameCTableCd +
         * node.getPath()); insertBreakPathStmt.execute(); }
         */
        if (node.getName().equals("Gender")) {
            insertBreakPathStmt.setString(1, "PATIENT_GENDER_COUNT_XML");
            insertBreakPathStmt.setString(2, "\\\\" + c_table_cd + node.getPath());
            insertBreakPathStmt.execute();
        }

        if (node.getName().equals("Race")) {
            insertBreakPathStmt.setString(1, "PATIENT_RACE_COUNT_XML");
            insertBreakPathStmt.setString(2, "\\\\" + c_table_cd + node.getPath());
            insertBreakPathStmt.execute();
        }

        if (node.getName().equals("Vital status")) {
            insertBreakPathStmt.setString(1, "PATIENT_VITALSTATUS_COUNT_XML");
            insertBreakPathStmt.setString(2, "\\\\" + c_table_cd + node.getPath());
            insertBreakPathStmt.execute();
        }

        if (node.getName().equals("Age")) {
            insertBreakPathStmt.setString(1, "PATIENT_AGE_COUNT_XML");
            insertBreakPathStmt.setString(2, "\\\\" + c_table_cd + node.getPath());
            insertBreakPathStmt.execute();
        }
    }
}
