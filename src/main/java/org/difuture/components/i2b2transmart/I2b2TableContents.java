/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart;

import java.util.HashMap;
import java.util.Map;

/**
 * The class I2b2TableContents.
 * 
 * Contains maps for the i2b2 table CODE_LOOKUP. The content of the maps
 * corresponds to the official demo data from i2b2.
 * 
 * @author Claudia Lang
 *
 */
public class I2b2TableContents {

    /**
     * A map for possible values for the demographic category religion. Contents
     * for i2b2demodata.code_lookup.
     * 
     * The map's key is used for code_cd and its value for name_char.
     */
    public final static Map<String, String> CODE_LOOKUP_PAT_REL  = new HashMap<String, String>() {

                                                                     /**
                                                                      * SVUID
                                                                      */
                                                                     private static final long serialVersionUID = 1L;

                                                                     {
                                                                         put("DEM|RELIGION:@",
                                                                             "Not Recorded-@");
                                                                         put("DEM|RELIGION:7a",
                                                                             "Seventh Day Adventist-7A");
                                                                         put("DEM|RELIGION:7th day advent",
                                                                             "Seventh Day Adventist-7TH DAY ADVENT");
                                                                         put("DEM|RELIGION:ac",
                                                                             "Advent Christian-AC");
                                                                         put("DEM|RELIGION:aca",
                                                                             "Armenian Catholic-ACA");
                                                                         put("DEM|RELIGION:acat",
                                                                             "Armenian Catholic-ACAT");
                                                                         put("DEM|RELIGION:advent christian",
                                                                             "Advent Christian-ADVENT CHRISTIAN");
                                                                         put("DEM|RELIGION:ag",
                                                                             "Agnostic-AG");
                                                                         put("DEM|RELIGION:agnostic",
                                                                             "Agnostic-AGNOSTIC");
                                                                         put("DEM|RELIGION:ai",
                                                                             "American Indian-AI");
                                                                         put("DEM|RELIGION:amer indian",
                                                                             "American Indian-AMER INDIAN");
                                                                         put("DEM|RELIGION:an",
                                                                             "Anglican-AN");
                                                                         put("DEM|RELIGION:anglican",
                                                                             "Anglican-ANGLICAN");
                                                                         put("DEM|RELIGION:armenian catholic",
                                                                             "Armenian Catholic-ARMENIAN CATHOLIC");
                                                                         put("DEM|RELIGION:as",
                                                                             "Assembly of God-AS");
                                                                         put("DEM|RELIGION:assemb of god",
                                                                             "Assembly of God-ASSEMB OF GOD");
                                                                         put("DEM|RELIGION:at",
                                                                             "Atheist-AT");
                                                                         put("DEM|RELIGION:ath",
                                                                             "Atheist-ATH");
                                                                         put("DEM|RELIGION:atheist",
                                                                             "Atheist-ATHEIST");
                                                                         put("DEM|RELIGION:ba",
                                                                             "Bahai-BA");
                                                                         put("DEM|RELIGION:bahai",
                                                                             "Bahai-BAHAI");
                                                                         put("DEM|RELIGION:baptist",
                                                                             "Baptist-BAPTIST");
                                                                         put("DEM|RELIGION:bp",
                                                                             "Baptist-BP");
                                                                         put("DEM|RELIGION:bu",
                                                                             "Buddhist-BU");
                                                                         put("DEM|RELIGION:buddhist",
                                                                             "Buddhist-BUDDHIST");
                                                                         put("DEM|RELIGION:ca",
                                                                             "Catholic-CA");
                                                                         put("DEM|RELIGION:car",
                                                                             "Charismatic-CAR");
                                                                         put("DEM|RELIGION:catholic",
                                                                             "Catholic-CATHOLIC");
                                                                         put("DEM|RELIGION:ch",
                                                                             "Christian-CH");
                                                                         put("DEM|RELIGION:charismatic",
                                                                             "Charismatic-CHARISMATIC");
                                                                         put("DEM|RELIGION:christian",
                                                                             "Christian-CHRISTIAN");
                                                                         put("DEM|RELIGION:christian science",
                                                                             "Christian Scientist-CHRISTIAN SCIENCE");
                                                                         put("DEM|RELIGION:church of england",
                                                                             "Church of England");
                                                                         put("DEM|RELIGION:church of god",
                                                                             "Church of God");
                                                                         put("DEM|RELIGION:co",
                                                                             "Congregational-CO");
                                                                         put("DEM|RELIGION:congragational",
                                                                             "Congregational-CONGRAGATIONAL");
                                                                         put("DEM|RELIGION:congregational",
                                                                             "Congregational-CONGREGATIONAL");
                                                                         put("DEM|RELIGION:cor",
                                                                             "Christian Orth");
                                                                         put("DEM|RELIGION:cs",
                                                                             "Christian Scientist-CS");
                                                                         put("DEM|RELIGION:dc",
                                                                             "Disciples Of Christ-DC");
                                                                         put("DEM|RELIGION:de",
                                                                             "Deferred-DE");
                                                                         put("DEM|RELIGION:deferred",
                                                                             "Deferred-DEFERRED");
                                                                         put("DEM|RELIGION:disciples of christ",
                                                                             "Disciples Of Christ-DISCIPLES OF CHRIST");
                                                                         put("DEM|RELIGION:eastern orthodox",
                                                                             "Eastern Orthodox-EASTERN ORTHODOX");
                                                                         put("DEM|RELIGION:ec",
                                                                             "Evangelical Christian-EC");
                                                                         put("DEM|RELIGION:eo",
                                                                             "Eastern Orthodox-EO");
                                                                         put("DEM|RELIGION:ep",
                                                                             "Episcopal-EP");
                                                                         put("DEM|RELIGION:episcopal",
                                                                             "Episcopal-EPISCOPAL");
                                                                         put("DEM|RELIGION:evang. christian",
                                                                             "Evangelical Christian-EVANG. CHRISTIAN");
                                                                         put("DEM|RELIGION:go",
                                                                             "Greek Orthodox-GO");
                                                                         put("DEM|RELIGION:greek orthodox",
                                                                             "Greek Orthodox-GREEK ORTHODOX");
                                                                         put("DEM|RELIGION:hi",
                                                                             "Hindu-HI");
                                                                         put("DEM|RELIGION:hindu",
                                                                             "Hindu-HINDU");
                                                                         put("DEM|RELIGION:is",
                                                                             "Islamic-IS");
                                                                         put("DEM|RELIGION:islamic",
                                                                             "Islamic-ISLAMIC");
                                                                         put("DEM|RELIGION:jehov. witness",
                                                                             "Jehovah's Witness-JEHOV. WITNESS");
                                                                         put("DEM|RELIGION:jehov.witness",
                                                                             "Jehovah's Witness-JEHOV.WITNESS");
                                                                         put("DEM|RELIGION:jewish",
                                                                             "Jewish");
                                                                         put("DEM|RELIGION:jh",
                                                                             "Jewish");
                                                                         put("DEM|RELIGION:jv",
                                                                             "Jehovah's Witness-JV");
                                                                         put("DEM|RELIGION:jw",
                                                                             "Jehovah's Witness-JW");
                                                                         put("DEM|RELIGION:lu",
                                                                             "Lutheran-LU");
                                                                         put("DEM|RELIGION:lutheran",
                                                                             "Lutheran-LUTHERAN");
                                                                         put("DEM|RELIGION:maronite catholic",
                                                                             "Maronite Catholic-MC");
                                                                         put("DEM|RELIGION:mc",
                                                                             "Maronite Catholic-MARONITE CATHOLIC");
                                                                         put("DEM|RELIGION:me",
                                                                             "Methodist-ME");
                                                                         put("DEM|RELIGION:melkite catholic",
                                                                             "Melkite Catholic-MELKITE CATHOLIC");
                                                                         put("DEM|RELIGION:mennonite",
                                                                             "Mennonite");
                                                                         put("DEM|RELIGION:methodist",
                                                                             "Methodist-METHODIST");
                                                                         put("DEM|RELIGION:mkc",
                                                                             "Melkite Catholic-MKC");
                                                                         put("DEM|RELIGION:mo",
                                                                             "Mormon-MO");
                                                                         put("DEM|RELIGION:mormon (lds)",
                                                                             "Mormon-MORMON (LDS)");
                                                                         put("DEM|RELIGION:ms",
                                                                             "Moslem");
                                                                         put("DEM|RELIGION:mu",
                                                                             "Muslim-MU");
                                                                         put("DEM|RELIGION:muslim",
                                                                             "Muslim-MUSLIM");
                                                                         put("DEM|RELIGION:na",
                                                                             "Nazarene-NAZ");
                                                                         put("DEM|RELIGION:naf",
                                                                             "Not Affiliated");
                                                                         put("DEM|RELIGION:nazarene",
                                                                             "Nazarene-NAZARENE");
                                                                         put("DEM|RELIGION:nd",
                                                                             "Non-denominational-ND");
                                                                         put("DEM|RELIGION:ndn",
                                                                             "Non-denominational-NDN");
                                                                         put("DEM|RELIGION:no",
                                                                             "None-NO");
                                                                         put("DEM|RELIGION:no pref",
                                                                             "No Preference-NO PREF");
                                                                         put("DEM|RELIGION:no visit",
                                                                             "No Visit");
                                                                         put("DEM|RELIGION:non-denom",
                                                                             "Non-denominational-NON-DENOM");
                                                                         put("DEM|RELIGION:none",
                                                                             "None-NONE");
                                                                         put("DEM|RELIGION:np",
                                                                             "Not Recorded-NP");
                                                                         put("DEM|RELIGION:nsp",
                                                                             "No Preference-NSP");
                                                                         put("DEM|RELIGION:oc",
                                                                             "Orthodox Christian-OC");
                                                                         put("DEM|RELIGION:or",
                                                                             "Orthodox-OR");
                                                                         put("DEM|RELIGION:orthodox",
                                                                             "Orthodox-ORTHODOX");
                                                                         put("DEM|RELIGION:orthodox christian",
                                                                             "Orthodox Christian-ORTHODOX CHRISTIAN");
                                                                         put("DEM|RELIGION:ot",
                                                                             "Other-OT");
                                                                         put("DEM|RELIGION:other",
                                                                             "Other-OTHER");
                                                                         put("DEM|RELIGION:pe",
                                                                             "Pentecostal-PE");
                                                                         put("DEM|RELIGION:pentecostal",
                                                                             "Pentecostal-PENTECOSTAL");
                                                                         put("DEM|RELIGION:pr",
                                                                             "Protestant-PR");
                                                                         put("DEM|RELIGION:presbyterian",
                                                                             "Presbyterian-PRESBYTERIAN");
                                                                         put("DEM|RELIGION:protestant",
                                                                             "Protestant-PROTESTANT");
                                                                         put("DEM|RELIGION:ps",
                                                                             "Presbyterian-PS");
                                                                         put("DEM|RELIGION:qu",
                                                                             "Quaker-QU");
                                                                         put("DEM|RELIGION:quaker",
                                                                             "Quaker-QUAKER");
                                                                         put("DEM|RELIGION:rc",
                                                                             "Roman Catholic-RC");
                                                                         put("DEM|RELIGION:ref",
                                                                             "Not Recorded-REF");
                                                                         put("DEM|RELIGION:religious science",
                                                                             "Religious Science-RS");
                                                                         put("DEM|RELIGION:ro",
                                                                             "Russian Orthodox-RO");
                                                                         put("DEM|RELIGION:roman catholic",
                                                                             "Roman Catholic-ROMAN CATHOLIC");
                                                                         put("DEM|RELIGION:rs",
                                                                             "Religious Science-RELIGIOUS SCIENCE");
                                                                         put("DEM|RELIGION:russian orth.",
                                                                             "Russian Orthodox-RUSSIAN ORTH.");
                                                                         put("DEM|RELIGION:sa",
                                                                             "Salvation Army-SA");
                                                                         put("DEM|RELIGION:salv. army",
                                                                             "Salvation Army-SALV. ARMY");
                                                                         put("DEM|RELIGION:sb",
                                                                             "Southern Baptist-SB");
                                                                         put("DEM|RELIGION:sci",
                                                                             "Scientology-SCI");
                                                                         put("DEM|RELIGION:scientology",
                                                                             "Scientology-SCIENTOLOGY");
                                                                         put("DEM|RELIGION:sd",
                                                                             "Seventh Day Adventist-SD");
                                                                         put("DEM|RELIGION:si",
                                                                             "Sikh-SI");
                                                                         put("DEM|RELIGION:sikh",
                                                                             "Sikh-SIKH");
                                                                         put("DEM|RELIGION:southern baptist",
                                                                             "Southern Baptist-SOUTHERN BAPTIST");
                                                                         put("DEM|RELIGION:sp",
                                                                             "Spiritualist-SP");
                                                                         put("DEM|RELIGION:spiritualist",
                                                                             "Spiritualist-SPIRITUALIST");
                                                                         put("DEM|RELIGION:swe",
                                                                             "Swedenborgia-SWE");
                                                                         put("DEM|RELIGION:swedenborgian",
                                                                             "Swedenborgian-SWEDENBORGIAN");
                                                                         put("DEM|RELIGION:ua",
                                                                             "Unaffiliated");
                                                                         put("DEM|RELIGION:ucc",
                                                                             "United Church of Christ-UCC");
                                                                         put("DEM|RELIGION:un",
                                                                             "Unknown-UN");
                                                                         put("DEM|RELIGION:unitarian",
                                                                             "Unitarian-UNITARIAN");
                                                                         put("DEM|RELIGION:unitarian univers.",
                                                                             "Unitarian Universal-UNITARIAN UNIVERS.");
                                                                         put("DEM|RELIGION:united church christ",
                                                                             "United Church of Christ-UNITED CHURCH CHRIST");
                                                                         put("DEM|RELIGION:united church of christ",
                                                                             "United Church of Christ-UNITED CHURCH  OF CHRIST");
                                                                         put("DEM|RELIGION:unknown",
                                                                             "Unknown-UNKNOWN");
                                                                         put("DEM|RELIGION:uu",
                                                                             "Unitarian-UU");
                                                                         put("DEM|RELIGION:uv",
                                                                             "Unitarian Universal-UV");
                                                                         put("DEM|RELIGION:wi",
                                                                             "Wiccan-WI");
                                                                         put("DEM|RELIGION:wiccan",
                                                                             "Wiccan-WICCAN");
                                                                         put("DEM|RELIGION:zor",
                                                                             "Zoroastrian-ZOR");
                                                                         put("DEM|RELIGION:zoroastrian",
                                                                             "Zoroastrian-ZOROASTRIAN");
                                                                     }
                                                                 };

    /**
     * A map for possible values for the demographic category sex. Contents for
     * i2b2demodata.code_lookup.
     * 
     * The map's key is used for code_cd and its value for name_char.
     */
    public final static Map<String, String> CODE_LOOKUP_PAT_SEX  = new HashMap<String, String>() {

                                                                     /**
                                                                      * SVUID
                                                                      */
                                                                     private static final long serialVersionUID = 1L;

                                                                     {
                                                                         put("DEM|SEX:@",
                                                                             "Unknown-@");
                                                                         put("DEM|SEX:f", "Female");
                                                                         put("DEM|SEX:m", "Male");
                                                                         put("DEM|SEX:u",
                                                                             "Unknown-U");
                                                                     }
                                                                 };

    /**
     * A map for possible values for the demographic category race. Contents for
     * i2b2demodata.code_lookup.
     * 
     * The map's key is used for code_cd and its value for name_char.
     */
    public final static Map<String, String> CODE_LOOKUP_PAT_RACE = new HashMap<String, String>() {

                                                                     /**
                                                                      * SVUID
                                                                      */
                                                                     private static final long serialVersionUID = 1L;

                                                                     {
                                                                         put("DEM|RACE:@",
                                                                             "Not Recorded-@");
                                                                         put("DEM|RACE:a",
                                                                             "Asian-A");
                                                                         put("DEM|RACE:al",
                                                                             "Aleutian-AL");
                                                                         put("DEM|RACE:aleutian",
                                                                             "Aleutian-ALEUTIAN");
                                                                         put("DEM|RACE:amer. indian",
                                                                             "American Indian-AMER. INDIAN");
                                                                         put("DEM|RACE:api",
                                                                             "Asian Pacific Islander-API");
                                                                         put("DEM|RACE:asian",
                                                                             "Asian-ASIAN");
                                                                         put("DEM|RACE:asian/pac. isl",
                                                                             "Asian Pacific Islander-ASIAN PAC ISL");
                                                                         put("DEM|RACE:b",
                                                                             "Black-B");
                                                                         put("DEM|RACE:black",
                                                                             "Black-BLACK");
                                                                         put("DEM|RACE:c",
                                                                             "White-C");
                                                                         put("DEM|RACE:d",
                                                                             "Not Recorded-D");
                                                                         put("DEM|RACE:deferred",
                                                                             "Not Recorded-DEFERRED");
                                                                         put("DEM|RACE:es",
                                                                             "Eskimo-ES");
                                                                         put("DEM|RACE:eskimo",
                                                                             "Eskimo-ESKIMO");
                                                                         put("DEM|RACE:h",
                                                                             "Hispanic-H");
                                                                         put("DEM|RACE:hib",
                                                                             "Hispanic Black-HIB");
                                                                         put("DEM|RACE:his/black",
                                                                             "Hispanic Black-HIS BLACK");
                                                                         put("DEM|RACE:his/white",
                                                                             "Hispanic White-HIS WHITE");
                                                                         put("DEM|RACE:hispanic",
                                                                             "Hispanic-HISPANIC");
                                                                         put("DEM|RACE:hiw",
                                                                             "Hispanic White-HIW");
                                                                         put("DEM|RACE:i",
                                                                             "American Indian-I");
                                                                         put("DEM|RACE:in",
                                                                             "Indian-IN");
                                                                         put("DEM|RACE:indian",
                                                                             "Indian-INDIAN");
                                                                         put("DEM|RACE:m",
                                                                             "Middle Eastern-M");
                                                                         put("DEM|RACE:mid.eastern",
                                                                             "Middle Eastern-MID EASTERN");
                                                                         put("DEM|RACE:mr",
                                                                             "Multiracial-MR");
                                                                         put("DEM|RACE:multi",
                                                                             "Multiracial-MULTI");
                                                                         put("DEM|RACE:na",
                                                                             "Native American-NA");
                                                                         put("DEM|RACE:na.esk",
                                                                             "Eskimo-NA ESK");
                                                                         put("DEM|RACE:nat. am.",
                                                                             "Native American-NAT AM");
                                                                         put("DEM|RACE:navajo",
                                                                             "Navajo-NAVAJO");
                                                                         put("DEM|RACE:ni",
                                                                             "Native American-NI");
                                                                         put("DEM|RACE:nv",
                                                                             "Navajo-NV");
                                                                         put("DEM|RACE:o",
                                                                             "Other-O");
                                                                         put("DEM|RACE:or",
                                                                             "Oriental-OR");
                                                                         put("DEM|RACE:oriental",
                                                                             "Oriental-ORIENTAL");
                                                                         put("DEM|RACE:other",
                                                                             "Other-OTHER");
                                                                         put("DEM|RACE:r",
                                                                             "Not Recorded-R");
                                                                         put("DEM|RACE:refused",
                                                                             "Not Recorded-REFUSED");
                                                                         put("DEM|RACE:u",
                                                                             "Not Recorded-U");
                                                                         put("DEM|RACE:unk",
                                                                             "Not Recorded-UNK");
                                                                         put("DEM|RACE:w",
                                                                             "White-W");
                                                                         put("DEM|RACE:white",
                                                                             "White-WHITE");
                                                                     }

                                                                 };

    /**
     * A map for possible values for the demographic category vital status.
     * Contents for i2b2demodata.code_lookup.
     * 
     * The map's key is used for code_cd and its value for name_char.
     */
    public final static Map<String, String> CODE_LOOKUP_PAT_VIT  = new HashMap<String, String>() {

                                                                     /**
                                                                      * SVUID
                                                                      */
                                                                     private static final long serialVersionUID = 1L;

                                                                     {
                                                                         put("DEM|VITAL:@",
                                                                             "Not recorded");
                                                                         put("DEM|VITAL:n",
                                                                             "Living");
                                                                         put("DEM|VITAL:x",
                                                                             "Deferred");
                                                                         put("DEM|VITAL:y",
                                                                             "Deceased");
                                                                     }
                                                                 };

    /**
     * A map for possible values for the demographic category language. Contents
     * for i2b2demodata.code_lookup.
     * 
     * The map's key is used for code_cd and its value for name_char.
     */
    public final static Map<String, String> CODE_LOOKUP_PAT_LANG = new HashMap<String, String>() {

                                                                     /**
                                                                      * SVUID
                                                                      */
                                                                     private static final long serialVersionUID = 1L;

                                                                     {
                                                                         put("DEM|LANGUAGE:@",
                                                                             "Not recorded-@");
                                                                         put("DEM|LANGUAGE:abo",
                                                                             "Aborigines-ABO");
                                                                         put("DEM|LANGUAGE:aborigines",
                                                                             "Aborigines-ABORIGINES");
                                                                         put("DEM|LANGUAGE:alba",
                                                                             "Albanian-ALBA");
                                                                         put("DEM|LANGUAGE:albanian",
                                                                             "Albanian-ALBANIAN");
                                                                         put("DEM|LANGUAGE:amer. sign lang",
                                                                             "American Sign Language-AMER. SIGN LANG");
                                                                         put("DEM|LANGUAGE:amha",
                                                                             "Ethiopian-Amharic-AMHA");
                                                                         put("DEM|LANGUAGE:arab",
                                                                             "Arabic-ARAB");
                                                                         put("DEM|LANGUAGE:arabic",
                                                                             "Arabic-ARABIC");
                                                                         put("DEM|LANGUAGE:arme",
                                                                             "Armenian-ARME");
                                                                         put("DEM|LANGUAGE:armenian",
                                                                             "Armenian-ARMENIAN");
                                                                         put("DEM|LANGUAGE:asl",
                                                                             "American Sign Language-ASL");
                                                                         put("DEM|LANGUAGE:beng",
                                                                             "Bengali-BENG");
                                                                         put("DEM|LANGUAGE:bengali",
                                                                             "Bengali-BENGALI");
                                                                         put("DEM|LANGUAGE:bosn",
                                                                             "Bosnian-BOSN");
                                                                         put("DEM|LANGUAGE:bosnian",
                                                                             "Bosnian-BOSNIAN");
                                                                         put("DEM|LANGUAGE:bulg",
                                                                             "Bulgarian-BULG");
                                                                         put("DEM|LANGUAGE:bulgarian",
                                                                             "Bulgarian-BULGARIAN");
                                                                         put("DEM|LANGUAGE:burm",
                                                                             "Burmese-BURM");
                                                                         put("DEM|LANGUAGE:burmese",
                                                                             "Burmese-BURMESE");
                                                                         put("DEM|LANGUAGE:camb",
                                                                             "Cambodian-CAMB");
                                                                         put("DEM|LANGUAGE:cambodian",
                                                                             "Cambodian-CAMBODIAN");
                                                                         put("DEM|LANGUAGE:cant",
                                                                             "Chinese-Cantonese-CANT");
                                                                         put("DEM|LANGUAGE:cape",
                                                                             "Cape Verdean-CAPE");
                                                                         put("DEM|LANGUAGE:cape verd-creole",
                                                                             "Cape Verdean Creole-CAPE VERD-CREOLE");
                                                                         put("DEM|LANGUAGE:cape verd-portuguese",
                                                                             "Cape Verdean Portugese-CAPE VERD-PORTUGUESE");
                                                                         put("DEM|LANGUAGE:cape verdean",
                                                                             "Cape Verdean-CAPE VERDEAN");
                                                                         put("DEM|LANGUAGE:capp",
                                                                             "Cape Verdean Portugese-CAPP");
                                                                         put("DEM|LANGUAGE:ccv",
                                                                             "Cape Verdean Creole-CCV");
                                                                         put("DEM|LANGUAGE:chin",
                                                                             "Chinese-CHIN");
                                                                         put("DEM|LANGUAGE:chinese",
                                                                             "Chinese-CHINESE");
                                                                         put("DEM|LANGUAGE:chinese-cant.",
                                                                             "Chinese-Cantonese-CHINESE-CANT.");
                                                                         put("DEM|LANGUAGE:chinese-mand.",
                                                                             "Chinese-Mandarin-CHINESE-MAND.");
                                                                         put("DEM|LANGUAGE:chinese-toisan",
                                                                             "Chinese-Toisan-CHINESE-TOISAN");
                                                                         put("DEM|LANGUAGE:creo",
                                                                             "Creole French");
                                                                         put("DEM|LANGUAGE:creole",
                                                                             "Creole");
                                                                         put("DEM|LANGUAGE:czech",
                                                                             "Czech-CZECH");
                                                                         put("DEM|LANGUAGE:czek",
                                                                             "Czech-CZEK");
                                                                         put("DEM|LANGUAGE:dane",
                                                                             "Danish-DANE");
                                                                         put("DEM|LANGUAGE:danish",
                                                                             "Danish-DANISH");
                                                                         put("DEM|LANGUAGE:dutc",
                                                                             "Dutch-DUTC");
                                                                         put("DEM|LANGUAGE:dutch",
                                                                             "Dutch-DUTCH");
                                                                         put("DEM|LANGUAGE:engl",
                                                                             "English-ENGL");
                                                                         put("DEM|LANGUAGE:english",
                                                                             "English-ENGLISH");
                                                                         put("DEM|LANGUAGE:erit",
                                                                             "Eritrean-ERIT");
                                                                         put("DEM|LANGUAGE:eritrean",
                                                                             "Eritrean-ERITREAN");
                                                                         put("DEM|LANGUAGE:ethiopian-amharic",
                                                                             "Ethiopian-Amharic-ETHIOPIAN-AMHARIC");
                                                                         put("DEM|LANGUAGE:ethiopian-tigrinya",
                                                                             "Ethiopian-Tigrinia-ETHIOPIAN-TIGRINYA");
                                                                         put("DEM|LANGUAGE:fars",
                                                                             "Farsi-FARS");
                                                                         put("DEM|LANGUAGE:farsi-persian",
                                                                             "Farsi-FARSI-PERSIAN");
                                                                         put("DEM|LANGUAGE:frc",
                                                                             "French Canadian");
                                                                         put("DEM|LANGUAGE:frcr",
                                                                             "French Creole-FRCR");
                                                                         put("DEM|LANGUAGE:fren",
                                                                             "French-FREN");
                                                                         put("DEM|LANGUAGE:french",
                                                                             "French-FRENCH");
                                                                         put("DEM|LANGUAGE:french creole",
                                                                             "French Creole-FRENCH CREOLE");
                                                                         put("DEM|LANGUAGE:germ",
                                                                             "German-GERM");
                                                                         put("DEM|LANGUAGE:german",
                                                                             "German-GERMAN");
                                                                         put("DEM|LANGUAGE:greek",
                                                                             "Greek-GREEK");
                                                                         put("DEM|LANGUAGE:grek",
                                                                             "Greek-GREK");
                                                                         put("DEM|LANGUAGE:guga",
                                                                             "Gujarat-GUGA");
                                                                         put("DEM|LANGUAGE:gugarati",
                                                                             "Gujarati-GUGARATI");
                                                                         put("DEM|LANGUAGE:haitian",
                                                                             "Haitian-HAITIAN");
                                                                         put("DEM|LANGUAGE:haitian-fr.creole",
                                                                             "Haitian French Creole-HAITIAN-FR.CREOLE");
                                                                         put("DEM|LANGUAGE:hati",
                                                                             "Haitian-HATI");
                                                                         put("DEM|LANGUAGE:hc",
                                                                             "Haitian French Creole-HC");
                                                                         put("DEM|LANGUAGE:hebr",
                                                                             "Hebrew-HEBR");
                                                                         put("DEM|LANGUAGE:hebrew",
                                                                             "Hebrew-HEBREW");
                                                                         put("DEM|LANGUAGE:hindi",
                                                                             "Hindi-HINDI");
                                                                         put("DEM|LANGUAGE:hmong",
                                                                             "Hmong-HMONG");
                                                                         put("DEM|LANGUAGE:hndi",
                                                                             "Hindi-HNDI");
                                                                         put("DEM|LANGUAGE:hung",
                                                                             "Hungarian-HUNG");
                                                                         put("DEM|LANGUAGE:hungarian",
                                                                             "Hungarian-HUNGARIAN");
                                                                         put("DEM|LANGUAGE:indian-native",
                                                                             "Native Indian-INDIAN-NATIVE");
                                                                         put("DEM|LANGUAGE:islandic",
                                                                             "Islandic-ISLANDIC");
                                                                         put("DEM|LANGUAGE:isle",
                                                                             "Islandic-ISLE");
                                                                         put("DEM|LANGUAGE:ital",
                                                                             "Italian-ITAL");
                                                                         put("DEM|LANGUAGE:italian",
                                                                             "Italian-ITALIAN");
                                                                         put("DEM|LANGUAGE:japa",
                                                                             "Japanese-JAPA");
                                                                         put("DEM|LANGUAGE:japanese",
                                                                             "Japanese-JAPANESE");
                                                                         put("DEM|LANGUAGE:kn",
                                                                             "KN");
                                                                         put("DEM|LANGUAGE:kore",
                                                                             "Korean-KORE");
                                                                         put("DEM|LANGUAGE:korean",
                                                                             "Korean-KOREAN");
                                                                         put("DEM|LANGUAGE:laot",
                                                                             "Laotian-LAOT");
                                                                         put("DEM|LANGUAGE:laotian",
                                                                             "Laotian-LAOTIAN");
                                                                         put("DEM|LANGUAGE:latv",
                                                                             "Latvian-LATV");
                                                                         put("DEM|LANGUAGE:latvian",
                                                                             "Latvian-LATVIAN");
                                                                         put("DEM|LANGUAGE:libe",
                                                                             "Liberian-LIBE");
                                                                         put("DEM|LANGUAGE:liberian",
                                                                             "Liberian-LIBERIAN");
                                                                         put("DEM|LANGUAGE:lith",
                                                                             "Lithuanian-LITH");
                                                                         put("DEM|LANGUAGE:lithuanian",
                                                                             "Lithuanian-LITHUANIAN");
                                                                         put("DEM|LANGUAGE:mand",
                                                                             "Chinese-Mandarin-MAND");
                                                                         put("DEM|LANGUAGE:mong",
                                                                             "Hmong-MONG");
                                                                         put("DEM|LANGUAGE:mute",
                                                                             "Mute / Illiterate");
                                                                         put("DEM|LANGUAGE:na",
                                                                             "Native Indian-NA");
                                                                         put("DEM|LANGUAGE:natv",
                                                                             "Native Indian-NATV");
                                                                         put("DEM|LANGUAGE:nav",
                                                                             "Navajo-NAV");
                                                                         put("DEM|LANGUAGE:navajo",
                                                                             "Navajo-NAVAJO");
                                                                         put("DEM|LANGUAGE:nez",
                                                                             "Nez Perce-NEZ");
                                                                         put("DEM|LANGUAGE:nez perce",
                                                                             "Nez Perce-NEZ PERCE");
                                                                         put("DEM|LANGUAGE:nige",
                                                                             "Nigerian-NIGE");
                                                                         put("DEM|LANGUAGE:nigerian",
                                                                             "Nigerian-NIGERIAN");
                                                                         put("DEM|LANGUAGE:norw",
                                                                             "Norwegian-NORW");
                                                                         put("DEM|LANGUAGE:norwegian",
                                                                             "Norwegian-NORWEGIAN");
                                                                         put("DEM|LANGUAGE:othe",
                                                                             "Other-OTHE");
                                                                         put("DEM|LANGUAGE:other",
                                                                             "Other-OTHER");
                                                                         put("DEM|LANGUAGE:pol",
                                                                             "Polish-POL");
                                                                         put("DEM|LANGUAGE:polish",
                                                                             "Polish-POLISH");
                                                                         put("DEM|LANGUAGE:por",
                                                                             "Portuguese-POR");
                                                                         put("DEM|LANGUAGE:port",
                                                                             "Portuguese-PORT");
                                                                         put("DEM|LANGUAGE:portuguese",
                                                                             "Portuguese-PORTUGUESE");
                                                                         put("DEM|LANGUAGE:prci",
                                                                             "Persian Farsi");
                                                                         put("DEM|LANGUAGE:punj",
                                                                             "Punjabi-PUNJ");
                                                                         put("DEM|LANGUAGE:punjabi",
                                                                             "Punjabi-PUNJABI");
                                                                         put("DEM|LANGUAGE:rom",
                                                                             "Romanian-ROM");
                                                                         put("DEM|LANGUAGE:romanian",
                                                                             "Romanian-ROMANIAN");
                                                                         put("DEM|LANGUAGE:russ",
                                                                             "Russian-RUSS");
                                                                         put("DEM|LANGUAGE:russian",
                                                                             "Russian-RUSSIAN");
                                                                         put("DEM|LANGUAGE:ser",
                                                                             "Serbo-Croation-SER");
                                                                         put("DEM|LANGUAGE:serbo-croatian",
                                                                             "Serbo-Croation-SERBO-CROATIAN");
                                                                         put("DEM|LANGUAGE:shnd",
                                                                             "Shonal Ndebele-SHND");
                                                                         put("DEM|LANGUAGE:shonal ndebele",
                                                                             "Shonal Ndebele-SHONAL NDEBELE");
                                                                         put("DEM|LANGUAGE:sign",
                                                                             "Sign Language-SIGN");
                                                                         put("DEM|LANGUAGE:sign lang",
                                                                             "Sign Language-SIGN LANG");
                                                                         put("DEM|LANGUAGE:soma",
                                                                             "Somalian-SOMA");
                                                                         put("DEM|LANGUAGE:somalian",
                                                                             "Somalian-SOMALIAN");
                                                                         put("DEM|LANGUAGE:span",
                                                                             "Spanish-SPAN");
                                                                         put("DEM|LANGUAGE:span-engl",
                                                                             "Spanish English-SPAN-ENGL");
                                                                         put("DEM|LANGUAGE:spanish",
                                                                             "Spanish-SPANISH");
                                                                         put("DEM|LANGUAGE:spen",
                                                                             "Spanish English-SPEN");
                                                                         put("DEM|LANGUAGE:swah",
                                                                             "Swahil-SWAH");
                                                                         put("DEM|LANGUAGE:swahili",
                                                                             "Swahil-SWAHILI");
                                                                         put("DEM|LANGUAGE:swed",
                                                                             "Swedish-SWED");
                                                                         put("DEM|LANGUAGE:swedish",
                                                                             "Swedish-SWEDISH");
                                                                         put("DEM|LANGUAGE:taga",
                                                                             "Tagalag-TAGA");
                                                                         put("DEM|LANGUAGE:tagalag",
                                                                             "Tagalag-TAGALAG");
                                                                         put("DEM|LANGUAGE:tagalog",
                                                                             "Tagalog");
                                                                         put("DEM|LANGUAGE:taiw",
                                                                             "Taiwanese-TAIW");
                                                                         put("DEM|LANGUAGE:taiwanese",
                                                                             "Taiwanese-TAIWANESE");
                                                                         put("DEM|LANGUAGE:tami",
                                                                             "Tamil-TAMI");
                                                                         put("DEM|LANGUAGE:tamil",
                                                                             "Tamil-TAMIL");
                                                                         put("DEM|LANGUAGE:test",
                                                                             "Not recorded-TEST");
                                                                         put("DEM|LANGUAGE:thai",
                                                                             "Thai");
                                                                         put("DEM|LANGUAGE:tibe",
                                                                             "Tibetan-TIBE");
                                                                         put("DEM|LANGUAGE:tibetan",
                                                                             "Tibetan-TIBETAN");
                                                                         put("DEM|LANGUAGE:tigr",
                                                                             "Ethiopian-Tigrinia-TIGR");
                                                                         put("DEM|LANGUAGE:tois",
                                                                             "Chinese-Toisan-TOIS");
                                                                         put("DEM|LANGUAGE:turk",
                                                                             "Turkish-TURK");
                                                                         put("DEM|LANGUAGE:turkish",
                                                                             "Turkish-TURKISH");
                                                                         put("DEM|LANGUAGE:u",
                                                                             "Not recorded-U");
                                                                         put("DEM|LANGUAGE:ukra",
                                                                             "Ukranian\\UKRA");
                                                                         put("DEM|LANGUAGE:ukrainian",
                                                                             "Ukranian\\UKRAINIAN");
                                                                         put("DEM|LANGUAGE:unknown",
                                                                             "Unknown-UNKNOWN");
                                                                         put("DEM|LANGUAGE:urdu",
                                                                             "Urdu");
                                                                         put("DEM|LANGUAGE:viet",
                                                                             "Vietnamese-VIET");
                                                                         put("DEM|LANGUAGE:vietnamese",
                                                                             "Vietnamese-VIETNAMESE");
                                                                         put("DEM|LANGUAGE:ydsh",
                                                                             "Yiddish-YDSH");
                                                                         put("DEM|LANGUAGE:yiddish",
                                                                             "Yiddish-YIDDISH");
                                                                         put("DEM|LANGUAGE:yugo",
                                                                             "Yugoslavian-YUGO");
                                                                         put("DEM|LANGUAGE:yugoslavian",
                                                                             "Yugoslavian-YUGOSLAVIAN");

                                                                     }
                                                                 };

    /**
     * A map for possible values for the observational fact.
     * 
     * The map's key is used for code_cd and its value for name_char.
     */
    public final static Map<String, String> CODE_LOOKUP_OBS_FACT = new HashMap<String, String>() {

                                                                     /**
                                                                      * SVUID
                                                                      */
                                                                     private static final long serialVersionUID = 1L;

                                                                     {
                                                                         put("0", "Admitting");
                                                                         put("1", "Primary");
                                                                         put("2", "Secondary");
                                                                     }
                                                                 };
}
