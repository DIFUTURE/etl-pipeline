/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.difuture.components.i2b2transmart.model.Key;
import org.difuture.components.util.LoggerUtil;

/**
 * The class ConfigurationKey. Configuration of concrete keys for subjects and
 * visits
 * 
 * @author Fabian Prasser
 * @author Claudia Lang
 * 
 */
public class ConfigurationKey implements Cloneable {

    /** The key attributes */
    private final List<String> attributes;
    /** Indexes */
    private List<Integer>      indexes;
    /** HL7 FHIR */
    private final String       source;
    /** HL7 FHIR */
    private final String       use;

    /**
     * Instantiates a new key. Source and use will be set to null.
     *
     * @param key
     *            - the key. May be a concatenation of strings.
     */
    public ConfigurationKey(List<String> key) {
        this(key, null, null);
    }

    /**
     * Instantiates a new key.
     *
     * @param key
     *            - the key. May be a concatenation of strings.
     * @param source
     * @param use
     */
    public ConfigurationKey(List<String> key, String source, String use) {
        this.attributes = key;
        this.indexes = new ArrayList<Integer>();
        this.source = source;
        this.use = use;
    }

    /**
     * Calculates the indexes
     * 
     * @param columnNames
     * @param
     */
    public void calculateIndexes(List<String> columnNames) throws IllegalStateException {
        if (!indexes.isEmpty()) {
            LoggerUtil.fatalUnchecked("Never call this method more than once!",
                                      ConfigurationKey.class,
                                      IllegalStateException.class);
        }
        if (attributes.isEmpty()) {
            LoggerUtil.fatalUnchecked("No attributes specified!",
                                      ConfigurationKey.class,
                                      IllegalStateException.class);
        }
        for (String key : attributes) {
            int index = columnNames.indexOf(key);
            if (index == -1) {
                LoggerUtil.info("Columns: " + columnNames.toString(), this.getClass());
                LoggerUtil.fatalUnchecked("Field " + key + " not available",
                                          this.getClass(),
                                          IllegalStateException.class);
            }
            indexes.add(index);
        }
    }

    /**
     * Gets a clone of configuration key and resets the indexes
     */
    @Override
    public ConfigurationKey clone() {
        return new ConfigurationKey(getKeyAttributes(), source, use);
    }

    /**
     * Creates a new key
     * 
     * @param row
     * @param source
     * @param use
     * @return a Key object
     */
    public Key getKey(List<String> row) {
        if (indexes.isEmpty()) {
            LoggerUtil.fatalUnchecked("Key definition has not been initialized!",
                                      ConfigurationKey.class,
                                      IllegalStateException.class);
        }
        List<String> values = new ArrayList<String>();
        for (int index : indexes) {
            values.add(row.get(index));
        }
        return new Key(values, source, use);
    }

    /**
     * Creates a new key
     * 
     * @param row
     * @param source
     * @param use
     * @return
     */
    public Key getKey(String[] row) {
        return getKey(Arrays.asList(row));
    }

    /**
     * Gets the attribute names of the keys, meaning the column names used for
     * the key are returned
     * 
     * @return a list
     */
    public List<String> getKeyAttributes() {
        return attributes;
    }

    /**
     * Applies the mapping
     * 
     * @param mappingColumns
     * @return
     */
    public ConfigurationKey map(Map<String, String> mappingColumns) {
        if (mappingColumns == null) { return this; }
        List<String> attributes = new ArrayList<>();
        for (String attribute : this.attributes) {
            attributes.add(mappingColumns.getOrDefault(attribute, attribute));
        }
        return new ConfigurationKey(attributes, source, use);
    }
}
