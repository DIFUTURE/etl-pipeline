/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

import java.util.Map;
import java.util.Map.Entry;

/**
 * Configuration for value mapping
 * 
 * @author Fabian Prasser
 */
public class ConfigurationValueMapping {

    /** Configuration */
    private final String              attribute;
    /** Configuration */
    private final Map<String, String> mapping;
    /** Configuration */
    private final Map<String, String> wildcard;
    /** Configuration */
    private final String              nonmatch;

    /**
     * Creates a new instance
     * 
     * @param attribute
     * @param mapping
     */
    public ConfigurationValueMapping(String attribute,
                                     Map<String, String> mapping,
                                     Map<String, String> wildcard,
                                     String nonmatch) {
        this.attribute = attribute;
        this.mapping = mapping;
        this.wildcard = wildcard;
        this.nonmatch = nonmatch;
    }

    /**
     * Returns the attribute
     * 
     * @return
     */
    public String getAttribute() {
        return this.attribute;
    }

    /**
     * Performs the mapping
     * 
     * @param input
     * @return
     */
    public String map(String input) {

        // Standard mapping
        String output = mapping == null ? null : mapping.get(input);

        // Wildcard mapping
        if (output == null) {
            output = getWildcardMatch(input);
        }

        // Default match
        if (output == null) {
            output = nonmatch == null ? input : nonmatch;
        }

        // Done
        return output;
    }

    /**
     * Returns a wildcard match
     * 
     * @param input
     * @return
     */
    private String getWildcardMatch(String input) {
        if (wildcard != null) {
            for (Entry<String, String> entry : wildcard.entrySet()) {
                if (input.contains(entry.getKey())) { return entry.getValue(); }
            }
        }
        return null;
    }
}
