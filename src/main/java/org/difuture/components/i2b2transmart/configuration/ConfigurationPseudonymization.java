/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

/**
 * Configuration for pseudonymization
 * 
 * @author Fabian Prasser
 */
public class ConfigurationPseudonymization {

    /** Configuration */
    public final String psnURL;
    /** Configuration */
    public final String authURL;
    /** Configuration */
    public final String authClient;
    /** Configuration */
    public final String authClientSecret;

    /**
     * Creates a new instance
     * 
     * @param psnURL
     * @param authURL
     * @param authClient
     * @param authClientSecret
     */
    public ConfigurationPseudonymization(String psnURL,
                                         String authURL,
                                         String authClient,
                                         String authClientSecret) {
        this.psnURL = psnURL;
        this.authURL = authURL;
        this.authClient = authClient;
        this.authClientSecret = authClientSecret;
    }
}
