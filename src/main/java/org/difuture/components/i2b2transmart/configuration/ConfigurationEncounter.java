/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

import java.io.File;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

/**
 * The class ConfigurationEncounter. Represents the configuration for the
 * encounter as configured in configuration.properties.
 * 
 * @author Fabian Prasser
 * @author Claudia Lang
 * 
 */
public class ConfigurationEncounter {

    /** File */
    public final File             file;
    /** Key - Encounter id */
    public final ConfigurationKey key;
    /** Start */
    public final String           start;
    /** End */
    public final String           end;
    /** Visit id */
    public final String           visitID;
    /** Date format */
    public final String           dateFormat;
    /** Formatter for the format */
    private DateTimeFormatter     dateFormatter = null;

    // i2b2 specific attributes
    /** activeStatus */
    public final String           activeStatus;
    /** inout */
    public final String           inout;
    /** location */
    public final String           location;
    /** location path */
    public final String           locationPath;
    /** length of stay */
    public final String           lengthOfStay;
    /** blob */
    public final String           blob;

    /**
     * Creates a new instance
     * 
     * @param file
     * @param key
     * @param start
     * @param end
     * @param visitID
     * @param encounter
     * @param dateFormat
     * @param activeStatus
     * @param inout
     * @param location
     * @param locationPath
     * @param lengthOfStay
     * @param blob
     */
    protected ConfigurationEncounter(File file,
                                     ConfigurationKey key,
                                     String start,
                                     String end,
                                     String visitID,
                                     String dateFormat,
                                     String activeStatus,
                                     String inout,
                                     String location,
                                     String locationPath,
                                     String lengthOfStay,
                                     String blob) {
        this.file = file;
        this.key = key;
        this.start = start;
        this.end = end;
        this.visitID = visitID;
        this.dateFormat = dateFormat;
        this.activeStatus = activeStatus;
        this.inout = inout;
        this.location = location;
        this.locationPath = locationPath;
        this.lengthOfStay = lengthOfStay;
        this.blob = blob;
    }

    /**
     * Returns a formatter for the date
     * 
     * @return
     */
    public DateTimeFormatter getDateFormatter() {
        if (dateFormat == null) {
            return null;
        } else {
            if (dateFormatter == null) {
                dateFormatter = DateTimeFormatter.ofPattern(dateFormat);
            }
            return dateFormatter;
        }
    }

    /**
     * Gets the name of the attributes to read from the input file for the
     * encounters.
     * 
     * @return a set
     */
    public Set<String> getSelectedColumns() {
        Set<String> result = new HashSet<>();
        result.addAll(key.getKeyAttributes());
        if (start != null) {
            result.add(start);
        }
        if (end != null) {
            result.add(end);
        }
        if (visitID != null) {
            result.add(visitID);
        }
        if (activeStatus != null) {
            result.add(activeStatus);
        }
        if (inout != null) {
            result.add(inout);
        }
        if (location != null) {
            result.add(location);
        }
        if (locationPath != null) {
            result.add(locationPath);
        }
        if (lengthOfStay != null) {
            result.add(lengthOfStay);
        }
        if (blob != null) {
            result.add(blob);
        }
        return result;

    }
}
