/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

import java.nio.charset.Charset;
import java.time.format.DateTimeFormatter;

/**
 * Contains static variables for the input and output files.
 * 
 * @author Claudia Lang
 * @author Fabian Prasser
 */
public class ConfigurationFileFormat {

    /** Create the single instance */
    private static final ConfigurationFileFormat INSTANCE = new ConfigurationFileFormat();

    /**
     * Returns the instance
     * 
     * @return
     */
    public static ConfigurationFileFormat getInstance() {
        return INSTANCE;
    }

    /** Output date format for staging files */
    private String            outputDateFormat;

    /** Output date format for staging files */
    private DateTimeFormatter outputDateFormatter;

    /** The delimiter to use for the output file. */
    private String            delimiterOutput;

    /** New line */
    private String            newLine;

    /** The charset used. */
    private Charset           charset;

    /** The maximum number of columns to read from a CSV file. */
    private int               maxcolsread;

    /**
     * Constructor
     */
    private ConfigurationFileFormat() {
        delimiterOutput = "\t";
        newLine = "\n";
        outputDateFormat = "yyyy-MM-dd'T'HH:mm:ss";
        outputDateFormatter = DateTimeFormatter.ofPattern(outputDateFormat);
    }

    /**
     * 
     * @return a Charset object
     */
    public Charset getCharset() {
        return charset;
    }

    /**
     * Returns the delimiter
     * 
     * @return
     */
    public String getDelimiterOutput() {
        return delimiterOutput;
    }

    /**
     * Get max cols to read
     * 
     * @return
     */
    public int getMaxcolsread() {
        return maxcolsread;
    }

    /**
     * Returns the newline character
     * 
     * @return
     */
    public String getNewLine() {
        return newLine;
    }

    /**
     * Returns the output date format
     * 
     * @return
     */
    public String getOutputDateFormat() {
        return outputDateFormat;
    }

    /**
     * Returns the output date formatter
     * 
     * @return
     */
    public DateTimeFormatter getOutputDateFormatter() {
        return outputDateFormatter;
    }

    /**
     * 
     * @param charset
     *            - a Charset object
     */
    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    /**
     * Set max cols to read
     * 
     * @param maxcolsread
     */
    public void setMaxcolsread(int maxcolsread) {
        this.maxcolsread = maxcolsread;
    }
}
