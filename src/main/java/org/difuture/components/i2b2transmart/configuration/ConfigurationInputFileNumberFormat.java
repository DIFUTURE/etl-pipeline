/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.util.LoggerUtil;

/**
 * Number converter
 * 
 * @author Fabian Prasser
 * @author Claudia Lang
 */
public class ConfigurationInputFileNumberFormat {

    /** Internal marker */
    private static final String INTERNAL = "ExcelDecimal:";

    /**
     * To internal number, used because we were not able to extract the
     * correctly formatted date from XLSX files using POI.
     * 
     * @param date
     * @return
     */
    public static String toInternalNumber(double number) {
        return INTERNAL + Double.toString(number);
    }

    /**
     * Converts an internal date
     * 
     * @param string
     * @return
     */
    private static String fromInternalNumber(String string) {
        if (string == null || string.length() == 0) {
            return "";
        } else {
            return string.substring(INTERNAL.length());
        }
    }

    /**
     * Is this an internal date
     * 
     * @param string
     * @return
     */
    private static boolean isInternalNumber(String string) {
        if (string == null || string.length() == 0) { return false; }
        return string.startsWith(INTERNAL);
    }

    /** Decimal format of the input files */
    private final DecimalFormat inputFormat;

    /** Decimal format of the output files */
    private final DecimalFormat outputFormat;

    /**
     * Create with default pattern
     */
    public ConfigurationInputFileNumberFormat() {
        this(null);
    }

    /**
     * Create with default locale
     * 
     * @param pattern
     */
    public ConfigurationInputFileNumberFormat(String pattern) {
        this(pattern, Locale.getDefault().getLanguage());
    }

    /**
     * Creates a new instance
     * 
     * @param pattern
     * @param ISOCode
     */
    public ConfigurationInputFileNumberFormat(String pattern, String ISOCode) {
        if (pattern != null && !"DEFAULT".equals(pattern)) {
            this.inputFormat = new DecimalFormat(pattern,
                                                 new DecimalFormatSymbols(getLocale(ISOCode)));
        } else {
            this.inputFormat = null;
        }

        // Decimal symbol
        DecimalFormatSymbols decimalSymbols = new DecimalFormatSymbols();
        decimalSymbols.setDecimalSeparator('.');

        // Decimal format
        this.outputFormat = new DecimalFormat();
        this.outputFormat.setGroupingUsed(false);
        this.outputFormat.setDecimalFormatSymbols(decimalSymbols);

        // Scale
        if (Model.get().isI2b2()) {
            // Set scale to 5.
            this.outputFormat.setMaximumFractionDigits(5);
        } else {
            // Set scale to maximum. Default value is 2.
            this.outputFormat.setMaximumFractionDigits(340);
        }
    }

    /**
     * Converts string to number and returns the number in the required format
     * as string. For i2b2, the decimals of the number are reduced to 5
     * (requiered by transmart-batch if loading into i2b2).
     * 
     * Returns empty string if the value cannot be parsed.
     * 
     * @param value
     * @param scale
     * @return
     */
    public String convert(String value) {

        // Convert string to number
        Number n = getNumber(value);

        // Return empty string
        if (n == null) { return ""; }

        // Format output
        return outputFormat.format(n);
    }

    /**
     * Convert string to number. Returns <code>null</code> if the value cannot
     * be parsed or if the value is empty or null.
     * 
     * @param value
     * @return
     */
    public Number getNumber(String value) {

        // Handle numbers parsed from Excel
        if (isInternalNumber(value)) { return Double.valueOf(fromInternalNumber(value)); }

        // Ignore
        if (value == null || value.equals("")) { return null; }

        // If default, use default parser
        if (inputFormat == null) {
            try {
                return Double.valueOf(value);
            } catch (Exception e) {
                return null;
            }
        }

        // If configured, use configured format
        try {
            return inputFormat.parse(value);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Check whether string is a number
     * 
     * @param value
     * @param decimalFormat
     * @return
     */
    public boolean isNumber(String value) {

        // We consider empty values to be numbers for now
        if (value == null || "".equals(value)) { return true; }

        // Check if we can parse this
        return (getNumber(value) != null);
    }

    /**
     * Converts a number to a string
     * 
     * @param tMean
     * @return
     */
    public String toString(Number tMean) {
        if (inputFormat == null) {
            return String.valueOf(tMean);
        } else {
            return inputFormat.format(tMean.doubleValue());
        }
    }

    /**
     * Gets the locale
     * 
     * @param ISOCode
     * @return
     */
    private Locale getLocale(String ISOCode) {

        for (Locale locale : Locale.getAvailableLocales()) {
            if (locale.getLanguage().equalsIgnoreCase(ISOCode)) { return locale; }
        }
        LoggerUtil.fatalUnchecked("Unknown ISO country code: " + ISOCode,
                                  this.getClass(),
                                  RuntimeException.class);
        return null;
    }
}
