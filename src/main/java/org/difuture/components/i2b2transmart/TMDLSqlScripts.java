/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.difuture.components.i2b2transmart.configuration.Configuration;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.util.LoggerUtil;

/**
 * Class for reading and executing sql statements needed to use tMDataLoader to
 * load data into transmart.
 * 
 * @author Claudia Lang
 *
 */
public class TMDLSqlScripts {

    /**
     * Deletes the user tm_dataloader and all the utility functions, tables, and
     * so on which are not required any more after loading process.
     * 
     * @param config
     * @throws SQLException
     * @throws IOException
     */
    public static void afterLoading() throws SQLException, IOException {
        // Prepare
        Configuration config = Model.get().getConfig();
        Connection conn = DriverManager.getConnection(config.getDatabaseConfiguration().getUrl(),
                                                      config.getDatabaseConfiguration().getUser(),
                                                      config.getDatabaseConfiguration()
                                                            .getPassword());
        List<String> folders = new ArrayList<>();
        folders.add("/sql-tmdl/postgres/after_load");

        // Run
        runSqlScripts(conn, folders);
    }

    /**
     * Runs all sql scripts required to load data to tranSMART via tMDataLoader
     * 
     * @throws SQLException
     * @throws IOException
     */
    public static void runSQLScripts() throws SQLException, IOException {
        // Prepare
        Configuration config = Model.get().getConfig();

        // Run sql scripts
        beforeLoading(config);
        runTmdlScripts(config);
    }

    /**
     * Creates the user tm_dataloader and runs several sql scripts, created by
     * tum imedis. tm_dataloader is required by tMDataLoader (unless you change
     * the scripts)
     * 
     * @param config
     * @throws SQLException
     * @throws IOException
     */
    private static void beforeLoading(Configuration config) throws SQLException, IOException {
        // Prepare
        Connection conn = DriverManager.getConnection(config.getDatabaseConfiguration().getUrl(),
                                                      config.getDatabaseConfiguration().getUser(),
                                                      config.getDatabaseConfiguration()
                                                            .getPassword());
        List<String> folders = new ArrayList<>();
        folders.add("/sql-tmdl/postgres/before_load");

        // Run
        runSqlScripts(conn, folders);
    }

    /**
     * 
     * @param conn
     * @param folders
     * @throws IOException
     * @throws SQLException
     */
    private static void runSqlScripts(Connection conn, List<String> folders) throws IOException,
                                                                             SQLException {
        List<String> folderNames = new ArrayList<>();
        String prefix = new File("").getAbsolutePath();
        for (String folder : folders) {
            folderNames.add(prefix + folder);
        }
        for (String folderName : folderNames) {
            File folder = new File(folderName);
            for (File fileEntry : folder.listFiles()) {
                if (fileEntry.isDirectory()) {
                    continue;
                }
                byte[] encoded = Files.readAllBytes(Paths.get(fileEntry.getAbsolutePath()));
                String query = new String(encoded);

                Statement stm = conn.createStatement();

                try {
                    stm.execute(query);
                } catch (SQLException e) {
                    LoggerUtil.info("SQLException occured while running script " +
                                    fileEntry.getPath() + "\n" + e.getMessage(),
                                    TMDLSqlScripts.class);
                    continue;
                }
            }
        }

        conn.close();
    }

    /**
     * Runs the sql scripts offered by tMDataLoader. Following scripts where
     * modified by tum imedis
     * 
     * - I2B2_BUILD_METADATA_XML - I2B2_LOAD_CLINICAL_DATA -
     * TIMESTAMP_TO_TIMEPOINT
     * 
     * @param config
     * @throws SQLException
     * @throws IOException
     */
    private static void runTmdlScripts(Configuration config) throws SQLException, IOException {
        // Prepare
        Connection conn = DriverManager.getConnection(config.getDatabaseConfiguration().getUrl(),
                                                      "tm_dataloader",
                                                      "tm_dataloader");
        List<String> folders = new ArrayList<>();
        folders.add("/sql-tmdl/postgres/misc");
        folders.add("/sql-tmdl/postgres/migrations/1_2_4");
        folders.add("/sql-tmdl/postgres/migrations/gwas_plink");
        folders.add("/sql-tmdl/postgres/migrations");
        folders.add("/sql-tmdl/postgres/procedures");

        // Run
        runSqlScripts(conn, folders);
    }

}
