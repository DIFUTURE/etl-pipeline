/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.model;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.difuture.components.i2b2transmart.configuration.Configuration;
import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;

/**
 * Model class
 * 
 * @author Fabian Prasser
 */
public class Model {

    /** Singleton */
    public static final Model INSTANCE = new Model();

    /**
     * Get the model
     * 
     * @return
     */
    public static Model get() {
        return INSTANCE;
    }

    /** Map with all subjects. */
    private final Map<Key, Subject>                   subjects        = new HashMap<>();

    /** Map with all visits. */
    private final Map<Key, Visit>                     visits          = new HashMap<>();

    /** Entities */
    private final Map<ConfigurationInputFile, Entity> entities        = new HashMap<>();

    /** Ontology class */
    private Ontology                                  ontology        = new Ontology();

    /** Is tMDataLoader to use */
    private boolean                                   tmDataLoader;

    /** Is the staging for I2B2 or not (tranSMART) */
    private boolean                                   i2b2;

    /** Contains the configuration. */
    private Configuration                             config;

    /** Can be used to lock access by key */
    private boolean                                   lockAccessByKey = false;

    /**
     * Subject data for tranSMART. Will be registered.
     * 
     * @param id
     * @param sex
     * @param zip
     * @param age
     * @param birthYear
     * @param birthMonth
     * @param birthDate
     */
    public Subject createSubject(Key id,
                                 String sex,
                                 String zip,
                                 AttributeAge age,
                                 Integer birthYear,
                                 Integer birthMonth,
                                 LocalDate birthDate) {
        return register(new Subject(id,
                                    sex,
                                    zip,
                                    age,
                                    birthYear,
                                    birthMonth,
                                    birthDate,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null));
    }

    /**
     * Subject data for i2b2. Will be registered.
     * 
     * @param id
     * @param sex
     * @param zip
     * @param age
     * @param birthYear
     * @param birthMonth
     * @param birthDate
     * @param vitalStatus
     *            only used in i2b2
     * @param language
     *            only used in i2b2
     * @param race
     *            only used in i2b2
     * @param maritalStatus
     *            only used in i2b2
     * @param religion
     *            only used in i2b2
     * @param stateCityZipPath
     *            only used in i2b2
     * @param income
     *            only used in i2b2
     * @param blob
     *            only used in i2b2
     */
    public Subject createSubject(Key id,
                                 String sex,
                                 String zip,
                                 AttributeAge age,
                                 Integer birthYear,
                                 Integer birthMonth,
                                 LocalDate birthDate,
                                 String vitalStatus,
                                 String language,
                                 String race,
                                 String maritalStatus,
                                 String religion,
                                 String stateCityZipPath,
                                 Integer income,
                                 String blob) {
        return register(new Subject(id,
                                    sex,
                                    zip,
                                    age,
                                    birthYear,
                                    birthMonth,
                                    birthDate,
                                    vitalStatus,
                                    language,
                                    race,
                                    maritalStatus,
                                    religion,
                                    stateCityZipPath,
                                    income,
                                    blob));

    }

    /**
     * Creates a new instance for tranSMART. Visit will be registered with the
     * list of visits.
     * 
     * @param id
     * @param name
     * @param start
     * @param end
     */
    public Visit createVisit(Key id, String name, Timestamp start, Timestamp end) {
        return register(new Visit(id, name, start, end, null, null, null, null, null, null));
    }

    /**
     * Creates a new instance for i2b2. Visit will be registered with the list
     * of visits.
     * 
     * @param id
     * @param name
     * @param start
     * @param end
     * @param activeStatus
     * @param inout
     * @param location
     * @param locationPath
     * @param lengthOfStay
     * @param blob
     */
    public Visit createVisit(Key id,
                             String name,
                             Timestamp start,
                             Timestamp end,
                             String activeStatus,
                             String inout,
                             String location,
                             String locationPath,
                             Integer lengthOfStay,
                             String blob) {
        return register(new Visit(id, name, start, end, null, null, null, null, null, null));
    }

    /**
     * Creates an artificial visit. Visit will be registered with the list of
     * visits.
     * 
     * @param subject
     * @return
     */
    public Visit createVisit(Subject subject) {
        return register(new Visit(subject.getId()));
    }

    /**
     * Get config
     * 
     * @return a Configuration object
     */
    public Configuration getConfig() {
        return config;
    }

    /**
     * Returns the entity metadata for this config
     * 
     * @param config
     * @return
     */
    public Entity getEntity(ConfigurationInputFile config) {
        return entities.get(config);
    }

    /**
     * Gets the size of the static map with all subjects.
     * 
     * @return an integer
     */
    public int getNumSubjects() {
        return subjects.size();
    }

    /**
     * Gets the number of all visits
     * 
     * @return - an integer value
     */
    public int getNumVisits() {
        return visits.size();
    }

    /**
     * Returns the ontology model
     * 
     * @return
     */
    public Ontology getOntology() {
        return this.ontology;
    }

    /**
     * Gets for a given key the subject of the static map with all subjects, if
     * available.
     * 
     * @param subjectID
     *            - a key object
     * @return a subject object
     */
    public Subject getSubject(Key subjectID) {
        if (lockAccessByKey) { throw new UnsupportedOperationException("Access by key is only supported while loading data. Keys may have changed in the meantime."); }
        return subjects.get(subjectID);
    }

    /**
     * Returns all subjects
     * 
     * @return
     */
    public Collection<Subject> getSubjects() {
        return subjects.values();
    }

    /**
     * Gets for a given key the visit of the map with all visits, if available.
     * 
     * @param visitID
     *            - a key object
     * @return a visit object
     */
    public Visit getVisit(Key visitID) {
        if (lockAccessByKey) { throw new UnsupportedOperationException("Access by key is only supported while loading data. Keys may have changed in the meantime."); }
        return visits.get(visitID);
    }

    /**
     * Gets an iterator for all visits
     * 
     * @return
     */
    public Collection<Visit> getVisits() {
        return visits.values();
    }

    /**
     * Return isI2b2
     * 
     * @return boolean
     */
    public boolean isI2b2() {
        return i2b2;
    }

    /**
     * Gets whether tMDataLoader is to use to load data into TranSMART
     * 
     * @return
     */
    public boolean isUseTmDataLoader() {
        return tmDataLoader;
    }

    /**
     * Locks access by key
     */
    public void lockAccessByKey() {
        this.lockAccessByKey = true;
    }

    /**
     * Removes a visit
     * 
     * @param visit
     */
    public void removeVisit(Visit visit) {
        // Cannot be implemented via the key, because the key might have changed
        // (pseudonymization)
        Iterator<Entry<Key, Visit>> iterator = visits.entrySet().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getValue().equals(visit)) {
                iterator.remove();
                return;
            }
        }
    }

    /**
     * Set config
     * 
     * @param config
     */
    public void setConfig(Configuration config) {
        this.config = config;
    }

    /**
     * Sets entity metadata
     * 
     * @param config
     * @param entity
     */
    public void setEntity(ConfigurationInputFile config, Entity entity) {
        this.entities.put(config, entity);
    }

    /**
     * Set isI2b2
     * 
     * @param i2b2
     *            - boolean
     */
    public void setI2b2(boolean i2b2) {
        this.i2b2 = i2b2;
    }

    /**
     * Sets whether tMDataLoader is to use to load data into TranSMART
     * 
     * @param tmDataLoader
     */
    public void setTmDataLoader(boolean tmDataLoader) {
        this.tmDataLoader = tmDataLoader;
    }

    /**
     * Registers a new instance
     * 
     * @param subject
     * @return
     */
    private Subject register(Subject subject) {

        // Register subject
        Subject existing = getSubject(subject.getId());
        if (existing != null) {
            existing.deduplicate(subject);
            return existing;
        } else {
            subjects.put(subject.getId(), subject);
            return subject;
        }
    }

    /**
     * Registers a visit
     * 
     * @param visit
     * @return
     */
    private Visit register(Visit visit) {

        // Register visit
        Visit existing = getVisit(visit.getId());
        if (existing != null) {
            existing.deduplicate(visit);
            return existing;
        } else {
            visits.put(visit.getId(), visit);
            return visit;
        }
    }
}
