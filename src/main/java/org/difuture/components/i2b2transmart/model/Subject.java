/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.configuration.ConfigurationSubject;

/**
 * Subject properties
 * 
 * @author Fabian Prasser
 * @author Claudia Lang
 */
public class Subject {

    /** Patient attribute */
    private Key          id;
    /** Visits for this patient */
    private Set<Visit>   visits        = new HashSet<>();

    /** Patient attribute */
    private AttributeAge age;
    /** Patient attribute */
    private Timestamp    dateFirstVisit;
    /** Patient attribute */
    private String       sex;
    /** Patient attribute */
    private String       zip;
    /** Patient attribute */
    private Integer      birthYear;
    /** Patient attribute */
    private Integer      birthMonth;
    /** Patient attribute */
    private LocalDate    birthDate;

    /** Patient attribute (i2b2 specific) */
    private String       vitalStatus;
    /** Patient attribute (i2b2 specific) */
    private String       language;
    /** Patient attribute (i2b2 specific) */
    private String       race;
    /** Patient attribute (i2b2 specific) */
    private String       maritalStatus;
    /** Patient attribute (i2b2 specific) */
    private String       religion;
    /** Patient attribute (i2b2 specific) */
    private String       statecityzipPath;
    /** Patient attribute (i2b2 specific) */
    private Integer      income;
    /** Patient attribute (i2b2 specific) */
    private String       blob;
    /** Has this subject already been pseudonymized */
    private boolean      pseudonymized = false;

    /**
     * Subject data for i2b2
     * 
     * @param id
     * @param sex
     * @param zip
     * @param age
     * @param birthYear
     * @param birthMonth
     * @param birthDate
     * @param vitalStatus
     *            only used in i2b2
     * @param language
     *            only used in i2b2
     * @param race
     *            only used in i2b2
     * @param maritalStatus
     *            only used in i2b2
     * @param religion
     *            only used in i2b2
     * @param statecityzipPath
     *            only used in i2b2
     * @param income
     *            only used in i2b2
     * @param blob
     *            only used in i2b2
     */
    Subject(Key id,
            String sex,
            String zip,
            AttributeAge age,
            Integer birthYear,
            Integer birthMonth,
            LocalDate birthDate,
            String vitalStatus,
            String language,
            String race,
            String maritalStatus,
            String religion,
            String statecityzipPath,
            Integer income,
            String blob) {
        this.id = id;
        this.sex = sex;
        this.zip = zip;
        this.age = age;
        this.birthYear = birthYear;
        this.birthMonth = birthMonth;
        this.birthDate = birthDate;
        this.vitalStatus = vitalStatus;
        this.language = language;
        this.race = race;
        this.maritalStatus = maritalStatus;
        this.religion = religion;
        this.statecityzipPath = statecityzipPath;
        this.income = income;
        this.blob = blob;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Subject other = (Subject) obj;
        return this.id.equals(other.id);
    }

    /**
     * Gets the age (either calculated or from data).
     * 
     * @return
     */
    public AttributeAge getAge() {
        return age;
    }

    /**
     * Gets the birth date.
     * 
     * @return
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }

    /**
     * Gets the birth month.
     * 
     * @return
     */
    public Integer getBirthMonth() {
        return birthMonth;
    }

    /**
     * Gets the birth year.
     * 
     * @return
     */
    public Integer getBirthYear() {
        return birthYear;
    }

    /**
     * Gets the blob.
     * 
     * @return
     */
    public String getBlob() {
        return blob;
    }

    /**
     * Gets the date of the first visit
     * 
     * @return - a Timestamp object
     */
    public Timestamp getDateFirstVisit() {
        return dateFirstVisit;
    }

    /**
     * Gets the subject id.
     * 
     * @return
     */
    public Key getId() {
        return id;
    }

    /**
     * Gets the income.
     * 
     * @return
     */
    public Integer getIncome() {
        return income;
    }

    /**
     * Gets the language.
     * 
     * @return
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Gets the marital status.
     * 
     * @return
     */
    public String getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * Gets the race.
     * 
     * @return
     */
    public String getRace() {
        return race;
    }

    /**
     * Gets the religion.
     * 
     * @return
     */
    public String getReligion() {
        return religion;
    }

    /**
     * Gets the sex.
     * 
     * @return
     */
    public String getSex() {
        return sex;
    }

    /**
     * Gets the label of sex to use in transmart.
     * 
     * @return sex
     */
    public String getSexTranSMART() {
        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();
        if (sex.equals(config.sexFemale)) return "female";
        if (sex.equals(config.sexMale)) return "male";
        if (sex.equals(config.sexUnknown)) return "unknown";
        return sex;
    }

    /**
     * Gets the state city zip path.
     * 
     * @return
     */
    public String getStateCityZipPath() {
        return statecityzipPath;
    }

    /**
     * Returns all visits for this subject
     * 
     * @return
     */
    public Set<Visit> getVisits() {
        return this.visits;
    }

    /**
     * Gets the vital status.
     * 
     * @return
     */
    public String getVitalStatus() {
        return vitalStatus;
    }

    /**
     * Gets zip.
     * 
     * @return
     */
    public String getZip() {
        return zip;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    /**
     * Returns whether the subject has been pseudonymized
     */
    public boolean isPseudonymized() {
        return this.pseudonymized;
    }

    /**
     * Sets the date of the first visit for a subject
     * 
     * @param dateFirstVisit
     */
    public void setDateFirstVisit(Timestamp dateFirstVisit) {
        this.dateFirstVisit = dateFirstVisit;
    }

    /**
     * Marks this subject as pseudonymized
     * 
     * @param pseudonymized
     */
    public void setPseudonymized(boolean pseudonymized) {
        this.pseudonymized = pseudonymized;
    }

    /**
     * Used by Spring Batch WriterItem to write data
     */
    public String toString() {
        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();

        StringBuilder string = new StringBuilder();
        String delimiter = ConfigurationFileFormat.getInstance().getDelimiterOutput();

        // Add studyID for tmDataLoader
        if (Model.get().isUseTmDataLoader()) {
            String studyID = Model.get().getConfig().getStudyID();
            string.append(studyID);
            string.append(delimiter);
        }

        string.append(id);

        if (config.sex != null) {
            string.append(delimiter);
            if (sex != null) {
                string.append(getSexTranSMART());
            }
        }

        if (config.zip != null) {
            string.append(delimiter);
            if (zip != null) {
                string.append(zip);
            }
        }

        // We are writing the age here
        // TODO: We don't support differentation between unknown and
        // not-recorded when writing the subject file
        if (config.age != null) {
            string.append(delimiter);
            string.append(age.isInvalid() || age.isMissing() ? "" : age.getValue());
        }

        // TODO: Not supported by i2b2.
        // Are birth year and birth month supported for transmart in a proper
        // way (see issue #59)?
        if (!Model.get().isI2b2()) {
            if (config.birthYear != null) {
                string.append(delimiter);
                if (birthYear != null) {
                    string.append(String.valueOf(birthYear));
                }
            }

            if (config.birthMonth != null) {
                string.append(delimiter);
                if (birthMonth != null) {
                    string.append(String.valueOf(birthMonth));
                }
            }
        }

        if (config.birthDate != null) {
            string.append(delimiter);
            if (birthDate != null) {
                if (Model.get().isI2b2()) {
                    string.append(birthDate.atStartOfDay()
                                           .format(ConfigurationFileFormat.getInstance()
                                                                          .getOutputDateFormatter()));
                } else {
                    string.append(birthDate.format(Model.get()
                                                        .getConfig()
                                                        .getSubjectConfiguration()
                                                        .getDateFormatter()));
                }
            }
        }

        if (config.vitalStatus != null) {
            string.append(delimiter);
            if (vitalStatus != null) {
                string.append(vitalStatus);
            }
        }

        if (config.language != null) {
            string.append(delimiter);
            if (language != null) {
                string.append(language);
            }
        }

        if (config.race != null) {
            string.append(delimiter);
            if (race != null) {
                string.append(race);
            }
        }

        if (config.maritalStatus != null) {
            string.append(delimiter);
            if (maritalStatus != null) {
                string.append(maritalStatus);
            }
        }

        if (config.religion != null) {
            string.append(delimiter);
            if (religion != null) {
                string.append(religion);
            }
        }

        if (config.stateCityZipPath != null) {
            string.append(delimiter);
            if (statecityzipPath != null) {
                string.append(statecityzipPath);
            }
        }

        if (config.income != null) {
            string.append(delimiter);
            if (income != null) {
                string.append(String.valueOf(income));
            }
        }

        if (config.blob != null) {
            string.append(delimiter);
            if (blob != null) {
                string.append(blob);
            }
        }

        if (config.loadIDs) {
            string.append(delimiter);
            string.append(this.id.toString());
        }
        return string.toString();
    }

    /**
     * Merges this subject with another subject returning a new subject
     * 
     * @param subject
     * @return
     */
    void deduplicate(Subject subject) {
        this.sex = subject.sex;
        this.zip = subject.zip;
        if (this.age.compareTo(subject.age) <= 0) {
            this.age = subject.age;
        }
        this.birthYear = subject.birthYear;
        this.birthMonth = subject.birthMonth;
        this.birthDate = subject.birthDate;
        this.vitalStatus = subject.vitalStatus;
        this.language = subject.language;
        this.race = subject.race;
        this.maritalStatus = subject.maritalStatus;
        this.religion = subject.religion;
        this.statecityzipPath = subject.statecityzipPath;
        this.income = subject.income;
        this.blob = subject.blob;
    }
}
