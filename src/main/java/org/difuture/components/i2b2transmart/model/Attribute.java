/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.model;

/**
 * Special class for values that are either (1) present and valid, (2) missing
 * or (3) have an unknown/invalid value
 * 
 * @author Claudia Lang
 * @author Fabian Prasser
 */
public abstract class Attribute<T> implements Comparable<Attribute<T>> {

    /** Unknown or invalid */
    public static final String UNKNOWN      = "Unknown";

    /** Missing */
    public static final String NOT_RECORDED = "Not recorded";

    /** The actual value: missing is represented by null */
    private T                  value;

    /** Whether this is an invalid value */
    private boolean            invalid      = false;

    /**
     * Creates a new instance with missing value.
     */
    public Attribute() {
        this.setValue((T) null);
    }

    /**
     * Creates a new instance.
     * 
     * @param value
     *            <code>null</code> means missing
     */
    public Attribute(String value) {
        this.setValue(value);
    }

    @Override
    public int compareTo(Attribute<T> other) {
        if (this.isInvalid() && other.isInvalid()) {
            return 0;
        } else if (this.isMissing() && other.isMissing()) {
            return 0;
        } else if (this.isInvalid() && other.isMissing()) {
            return -1;
        } else if (this.isMissing() && other.isInvalid()) {
            return +1;
        } else return compare(other.getValue());
    }

    /**
     * Gets the value
     * 
     * @return
     */
    public T getValue() {
        return value;
    }

    /**
     * Returns the value as a string
     * 
     * @return
     */
    public String getValueAsString() {
        if (this.isInvalid()) {
            return UNKNOWN;
        } else if (this.isMissing()) {
            return NOT_RECORDED;
        } else {
            return String.valueOf(this.getValue());
        }
    }

    /** Is value unknown */
    public boolean isInvalid() {
        return invalid;
    }

    /** Is value missing */
    public boolean isMissing() {
        return isInvalid() ? false : value == null;
    }

    /**
     * Sets the value
     * 
     * @param value
     */
    public void setValue(String value) {

        // Missing
        if (value.equals("")) {
            this.value = null;
            this.invalid = false;

            // Parse
        } else {
            try {
                this.value = value == null ? null : parse(value);
                this.invalid = value == null ? false : !this.isValid(this.value);
            } catch (Exception e) {
                this.invalid = true;
            }
        }
    }

    /**
     * Sets the value
     * 
     * @param value
     */
    public void setValue(T value) {
        this.value = value;
        this.invalid = value == null ? false : !this.isValid(this.value);
    }

    /**
     * Comparator
     * 
     * @param value
     * @return
     */
    protected abstract int compare(T value);

    /**
     * Returns whether the value is valid
     * 
     * @param value
     * @return
     */
    protected abstract boolean isValid(T value);

    /**
     * Parses the value
     * 
     * @param value
     * @return
     */
    protected abstract T parse(String value);
}
