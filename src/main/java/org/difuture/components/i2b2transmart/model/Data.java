/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Attributes derived from a specific file for a specific visit
 * 
 * @author Fabian Prasser
 */
public class Data {

    /** Values: attribute name -> value */
    private Map<String, String> values = new HashMap<String, String>();
    /** Start time */
    private Timestamp           start;
    /** End time */
    private Timestamp           end;

    /**
     * Creates a new instance without timestamps
     */
    public Data() {
        this.start = null;
        this.end = null;
    }

    /**
     * Creates a new instance with timestamps
     */
    public Data(Timestamp start, Timestamp end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Data other = (Data) obj;
        if (end == null) {
            if (other.end != null) return false;
        } else if (!end.equals(other.end)) return false;
        if (start == null) {
            if (other.start != null) return false;
        } else if (!start.equals(other.start)) return false;
        if (values == null) {
            if (other.values != null) return false;
        } else if (!values.equals(other.values)) return false;
        return true;
    }

    /**
     * Returns the end timestamp
     * 
     * @return
     */
    public Timestamp getEnd() {
        return end;
    }

    /**
     * Returns the start timestamp
     * 
     * @return
     */
    public Timestamp getStart() {
        return start;
    }

    /**
     * Returns the values
     * 
     * @return
     */
    public Map<String, String> getValues() {
        return values;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((end == null) ? 0 : end.hashCode());
        result = prime * result + ((start == null) ? 0 : start.hashCode());
        result = prime * result + ((values == null) ? 0 : values.hashCode());
        return result;
    }

    /**
     * Returns whether this is empty
     * 
     * @return
     */
    public boolean isEmpty() {
        for (String value : values.values()) {
            if (!value.equals("")) { return false; }
        }
        return true;
    }

    /**
     * Puts a data point
     * 
     * @param attribute
     * @param value
     */
    public void put(String attribute, String value) {
        this.values.put(attribute, value);
    }

    /**
     * Sets the end timestamp
     * 
     * @param end
     */
    public void setEnd(Timestamp end) {
        this.end = end;
    }

    /**
     * Sets the start timestamp
     * 
     * @param start
     */
    public void setStart(Timestamp start) {
        this.start = start;
    }
}
