/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.model;

import java.util.ArrayList;
import java.util.List;

import org.difuture.components.util.LoggerUtil;

/**
 * A concrete key
 * 
 * @author Fabian Prasser
 * @author Claudia Lang
 */
public class Key implements Cloneable {

    /** The values */
    private final List<String> values;
    /** Source HL7 FHIR */
    private String             system;
    /** Use HL7 FHIR */
    private String             use;

    /**
     * Creates a new instace
     * 
     * @param values
     * @param system
     * @param use
     */
    public Key(List<String> values, String system, String use) {
        this.values = values;
        this.system = system;
        this.use = use;
    }

    @Override
    public Key clone() throws CloneNotSupportedException {
        return (Key) super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Key)) { return false; }
        return this.values.equals(((Key) obj).values);
    }

    /**
     * Returns an artificial visit key for this subject key
     * 
     * @return
     */
    public Key getArtificalVisitKey() {
        List<String> values = new ArrayList<>(this.values);
        values.add(0, Visit.NAME_ARTIFICIAL_VISIT);
        return new Key(values, this.system, this.use);
    }

    /**
     * Return subject key for artificial visit
     * 
     * @return
     */
    public Key getSubjectKey() {
        if (!this.isArtificial()) {
            LoggerUtil.fatalUnchecked("This key is not artificial",
                                      this.getClass(),
                                      IllegalStateException.class);
        }
        // Return remainder
        return new Key(this.values.subList(1, this.values.size()), this.system, this.use);
    }

    /**
     * Returns the identifier source (HL7 FHIR)
     * 
     * @return
     */
    public String getSystem() {
        return this.system != null ? this.system : "Unknown";
    }

    @Override
    public int hashCode() {
        return values.hashCode();
    }

    /**
     * Is this an artificial id?
     * 
     * @return
     */
    public boolean isArtificial() {
        return this.values.get(0).equals(Visit.NAME_ARTIFICIAL_VISIT);
    }

    /**
     * Used for pseudonymization
     */
    public void setSubjectKey(Key key) {
        if (!this.isArtificial()) {
            LoggerUtil.fatalUnchecked("This key is not artificial",
                                      this.getClass(),
                                      IllegalStateException.class);
        }
        this.values.clear();
        this.values.addAll(key.values);
        this.values.add(0, Visit.NAME_ARTIFICIAL_VISIT);
    }

    /**
     * Update
     * 
     * @param value
     */
    public void setSystem(String value) {
        this.system = value;
    }

    /**
     * Update
     * 
     * @param value
     */
    public void setValue(String value) {
        if (this.isArtificial()) { throw new IllegalStateException("Artificial IDs may not be updated"); }
        this.values.clear();
        this.values.add(value);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (String value : values) {
            builder.append(value);
        }
        return builder.toString();
    }
}
