/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.model;

/**
 * Age can be either valid, invalid or missing
 * 
 * @author prasser
 *
 */
public class AttributeAge extends Attribute<Integer> {

    /** Maximum age supported */
    public static final int MAX_VALUE = 125;

    /**
     * Creates a new instance with missing value.
     */
    public AttributeAge() {
        super();
    }

    /**
     * Creates a new instance
     * 
     * @param value
     */
    protected AttributeAge(String value) {
        super(value);
    }

    @Override
    protected int compare(Integer value) {
        if (this.getValue() == null && value == null) { return 0; }
        if (this.getValue() != null && value == null) { return 1; }
        if (this.getValue() == null && value != null) { return -1; }
        return this.getValue().compareTo(value);
    }

    @Override
    protected boolean isValid(Integer value) {
        return (value >= 0 && value <= MAX_VALUE);
    }

    @Override
    protected Integer parse(String value) {
        return Integer.valueOf(value);
    }
}
