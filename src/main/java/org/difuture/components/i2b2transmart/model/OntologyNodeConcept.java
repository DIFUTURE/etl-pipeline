/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.lang.StringEscapeUtils;
import org.difuture.components.util.LoggerUtil;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This class represents a node in i2b2's concept hierarchy. Subclassed by
 * ConceptContainerNode, ConceptFolderNode, and ConceptLeafNode
 * 
 * @author Helmut Spengler
 * @author Claudia Lang
 * @author Fabian Prasser
 */
public class OntologyNodeConcept {

    /** Max fanout */
    private static final int            MAX_FANOUT   = 100;

    /** Counts per concept prefix */
    private static Map<String, Integer> CODE_COUNT   = new HashMap<String, Integer>();

    /** Generic XML template for numeric concepts */
    private static final String         XML_TEMPLATE = "<?xml version=\"1.0\"?>" +
                                                       "<ValueMetadata>" +
                                                       "   <Version>3.02</Version>" +
                                                       "   <CreationDateTime>XML_TOKEN_CREATION_DATE</CreationDateTime>" +
                                                       "   <TestID>XML_TOKEN_ID</TestID>" +
                                                       "   <TestName>XML_TOKEN_TEXT</TestName>" +
                                                       "   <DataType>Float</DataType>" +
                                                       "   <CodeType>XML_TOKEN_CODETYPE</CodeType>" +
                                                       "   <Loinc>XML_TOKEN_LOINC</Loinc>" +
                                                       "   <Flagstouse>HL</Flagstouse>" +
                                                       "   <Oktousevalues>Y</Oktousevalues>" +
                                                       "   <MaxStringLength/>" +
                                                       "   <LowofLowValue/>" +
                                                       "   <HighofLowValue/>" +
                                                       "   <LowofHighValue/>" +
                                                       "   <HighofHighValue/>" +
                                                       "   <LowofToxicValue/>" +
                                                       "   <HighofToxicValue/>" +
                                                       "   <EnumValues/>" +
                                                       "   <CommentsDeterminingExclusion>" +
                                                       "       <Com/>" +
                                                       "   </CommentsDeterminingExclusion>" +
                                                       "   <UnitValues>" +
                                                       "       <NormalUnits> </NormalUnits>" +
                                                       "       <EqualUnits> </EqualUnits>" +
                                                       "       <ExcludingUnits/>" +
                                                       "       <ConvertingUnits>" +
                                                       "           <Units/>" +
                                                       "           <MultiplyingFactor/>" +
                                                       "       </ConvertingUnits>" +
                                                       "   </UnitValues>" + "   <Analysis>" +
                                                       "       <Enums />" + "       <Counts />" +
                                                       "       <New />" + "   </Analysis>" +
                                                       "</ValueMetadata>";

    /**
     * Reads an ontology from an XML file
     * 
     * @param file
     * @return
     * @throws IOException
     */
    public static OntologyNodeConcept deserialize(File file) throws IOException {

        List<OntologyNodeConcept> root = new ArrayList<>();

        try {

            Stack<OntologyNodeConcept> stack = new Stack<>();
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            SAXParser saxParser = saxParserFactory.newSAXParser();
            saxParser.parse(file, new DefaultHandler() {

                @Override
                public void
                       endElement(String uri, String localName, String qName) throws SAXException {
                    if (qName.equals("node")) {
                        root.clear();
                        root.add(stack.pop());
                    }
                }

                @Override
                public void startElement(String uri,
                                         String localName,
                                         String qName,
                                         Attributes attributes) throws SAXException {
                    if (qName.equals("node")) {
                        OntologyNodeConcept node;
                        String type = attributes.getValue("type");
                        String name = StringEscapeUtils.unescapeXml(attributes.getValue("name"));
                        String attribute = StringEscapeUtils.unescapeXml(attributes.getValue("name"));
                        String code = StringEscapeUtils.unescapeXml(attributes.getValue("code"));
                        String target = StringEscapeUtils.unescapeXml(attributes.getValue("target"));
                        String hideNode = attributes.getValue("hide");
                        boolean numeric = Boolean.valueOf(attributes.getValue("numeric"));
                        if (type.equals("concept")) {
                            node = new OntologyNodeConcept(stack.isEmpty() ? null
                                    : stack.peek(), name, code, numeric, "true".equals(attribute));
                            node.hide = hideNode.equals("true");
                            stack.push(node);
                        } else { // "modifier"
                            node = new OntologyNodeModifier(stack.peek(),
                                                            name,
                                                            code,
                                                            numeric,
                                                            target);
                            stack.push(node);
                        }
                    }
                }
            });
        } catch (Exception e) {
            LoggerUtil.fatalChecked(e, OntologyNodeConcept.class, IOException.class);
        }

        // Return
        return root.get(0);
    }

    /**
     * Whether to hide this node or not. Hidden nodes are not written to i2b2.
     * The default is false.
     */
    private boolean                         hide      = false;

    /** Whether this node represents an attribute. Used for packing */
    private boolean                         attribute = false;

    /** The name of the concept */
    private final String                    name;

    /** The children of this node */
    private final List<OntologyNodeConcept> children  = new ArrayList<>();

    /** Is this a numeric concept? */
    private final boolean                   numeric;

    /** Parent */
    private OntologyNodeConcept             parent;

    /** Path */
    private String                          path;

    /** Id */
    private final String                    code;

    /** Depth */
    private int                             depth;

    /**
     * Creates a new instance which is not an attribute
     * 
     * @param parent
     * @param name
     * @param code
     * @param numeric
     */
    public OntologyNodeConcept(OntologyNodeConcept parent,
                               String name,
                               String code,
                               boolean numeric) {
        this(parent, name, code, numeric, false);
    }

    /**
     * Creates a new instance
     * 
     * @param parent
     * @param name
     * @param code
     * @param numeric
     * @param attribute
     */
    public OntologyNodeConcept(OntologyNodeConcept parent,
                               String name,
                               String code,
                               boolean numeric,
                               boolean attribute) {
        if (parent == null) {
            this.parent = null;
            this.path = "\\" + name + "\\";
            this.depth = 1;
        } else {
            this.parent = parent;
            this.parent.children.add(this);
            this.path = this.parent.getPath() + name + "\\";
            this.depth = this.parent.getDepth() + 1;
        }
        this.name = name;
        this.numeric = numeric;
        this.code = code;
        this.attribute = attribute;
    }

    /**
     * Creates a new instance which is not an attribute
     * 
     * @param prefix
     * @param parent
     * @param name
     * @param numeric
     */
    public OntologyNodeConcept(String prefix,
                               OntologyNodeConcept parent,
                               String name,
                               boolean numeric) {
        this(prefix, parent, name, numeric, false);
    }

    /**
     * Creates a new instance
     * 
     * @param prefix
     * @param parent
     * @param name
     * @param numeric
     * @param attribute
     */
    public OntologyNodeConcept(String prefix,
                               OntologyNodeConcept parent,
                               String name,
                               boolean numeric,
                               boolean attribute) {
        this.name = name;
        this.numeric = numeric;
        this.parent = parent;
        this.parent.children.add(this);
        this.path = this.parent.getPath() + this.name + "\\";
        this.depth = this.parent.getDepth() + 1;
        this.code = getCode(prefix);
        this.attribute = attribute;
    }

    /**
     * Creates a new root node
     * 
     * @param prefix
     * @param name
     */
    public OntologyNodeConcept(String prefix, String name) {
        this.name = name;
        this.numeric = false;
        this.parent = null;
        this.path = "\\" + this.name + "\\";
        this.depth = 0;
        this.code = getCode(prefix);
        this.attribute = false;
    }

    /**
     * @return the children of this node
     */
    public List<OntologyNodeConcept> getChildren() {
        return children;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the datatype of this node
     */
    public String getDataType() {
        return numeric ? "N" : "T";
    }

    /**
     * Returns the depth
     * 
     * @return
     */
    public int getDepth() {
        return this.depth;
    }

    /**
     * Returns the full name. For concepts this is the path and for modifiers it
     * is the postfix of the path
     * 
     * @return
     */
    public String getFullName() {
        return getPath();
    }

    /**
     * Gets the information whether to hide this node or not. Hidden nodes are
     * not written to i2b2.
     * 
     * @return
     */
    public boolean getHide() {
        return hide;
    }

    /**
     * @return the name of this node
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the path
     * 
     * @return
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Returns the size of the ontology
     * 
     * @return
     */
    public int getSize() {
        int size = 1;
        for (OntologyNodeConcept child : children) {
            size += child.getSize();
        }
        return size;
    }

    /**
     * Returns the type of the given node
     * 
     * @param nodeConcept
     * @return
     */
    public String getType(OntologyNodeConcept nodeConcept) {
        return (this instanceof OntologyNodeModifier) ? "modifier" : "concept";
    }

    /**
     * @return the DB code for the visual representation of this node
     */
    public String getVisualAttributes() {
        return (isLeaf() ? "L" : "F") + (hide ? "H" : "A");
    }

    /**
     * Returns XML for this concept, null if it is not numeric
     * 
     * @return
     */
    public String getXML() {
        return !numeric ? null
                : XML_TEMPLATE.replaceAll("XML_TOKEN_CREATION_DATE",
                                          new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()))
                              .replaceAll("XML_TOKEN_ID", name)
                              .replaceAll("XML_TOKEN_TEXT", name);
    }

    /**
     * @return whether this node is a leaf
     */
    public boolean isLeaf() {
        return children.isEmpty();
    }

    /**
     * Returns whether this is a numeric node
     * 
     * @return
     */
    public boolean isNumeric() {
        return this.numeric;
    }

    /**
     * Prepare before saving
     */
    public void pack() {

        // Determine whether this is an entity
        boolean isEntity = false;
        for (OntologyNodeConcept child : this.children) {
            if (child.attribute) {
                isEntity = true;
                break;
            }
        }

        // If this is an entity, pack
        if (isEntity) {
            sort();
            for (OntologyNodeConcept child : this.children) {
                while (child.reduceFanout(MAX_FANOUT)) {
                    // Empty by design
                }
            }

            // Else, go deeper into the tree
        } else {
            for (OntologyNodeConcept child : this.children) {
                child.pack();
            }
        }
    }

    /**
     * Serialize as XML
     * 
     * @param file
     * @throws IOException
     */
    public void serialize(File file) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write("<ontology>\n");
        this.serialize(writer, "   ");
        writer.write("</ontology>\n");
        writer.close();
    }

    /**
     * Sets whether to hide this node or not. Hidden nodes are not written to
     * i2b2.
     * 
     * @param hide
     *            - a boolean
     */
    public void setHide(boolean hide) {
        this.hide = hide;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        this.toString(b, "");
        return b.toString();
    }

    /**
     * Returns a code for the concept
     * 
     * @param prefix
     * @return
     */
    private String getCode(String prefix) {
        prefix = prefix.substring(0, Math.min(10, prefix.length())).toUpperCase();
        Integer count = CODE_COUNT.get(prefix);
        count = count == null ? 0 : count + 1;
        CODE_COUNT.put(prefix, count);
        return prefix + ":" + String.valueOf(count);
    }

    /**
     * Get leaf
     * 
     * @param nodes
     * @param right
     * @return
     */
    private OntologyNodeConcept getLeaf(List<OntologyNodeConcept> nodes, boolean right) {
        OntologyNodeConcept node = right ? nodes.get(nodes.size() - 1) : nodes.get(0);
        if (node.isLeaf()) {
            return node;
        } else {
            return getLeaf(node.children, right);
        }
    }

    /**
     * Returns the target, if any
     * 
     * @param nodeConcept
     * @return
     */
    private String getTarget(OntologyNodeConcept nodeConcept) {
        if (this instanceof OntologyNodeModifier) {
            return ((OntologyNodeModifier) this).getTargetPath();
        } else {
            return "";
        }
    }

    /**
     * Merges a set of nodes
     * 
     * @param nodes
     * @return
     */
    private OntologyNodeConcept merge(List<OntologyNodeConcept> nodes) {

        String min = getLeaf(nodes, false).name;
        String max = getLeaf(nodes, true).name;

        // Create new node
        OntologyNodeConcept result = new OntologyNodeConcept("ARTIFICIAL_INNER",
                                                             this,
                                                             "[" + min + " - " + max + "]",
                                                             false,
                                                             false);
        result.children.addAll(nodes);

        // Change properties of children
        for (OntologyNodeConcept child : nodes) {
            child.parent = result;
            child.depth++;
            child.path = result.getPath() + child.name + "\\";

            // TODO: child may have children as well, their path needs to be
            // adjusted, too.
        }

        // Return new inner
        return result;
    }

    /**
     * Reduce fanout of ontology nodes by inserting intermediate levels in
     * hierarchies (top-down)
     * 
     * @param fanout
     * @return whether changes were made
     */
    private boolean reduceFanout(int fanout) {

        // Create new list of children
        List<OntologyNodeConcept> oldChildren = new ArrayList<>(children);
        List<OntologyNodeConcept> newChildren = new ArrayList<>();
        List<OntologyNodeConcept> newModifiers = new ArrayList<>();
        List<OntologyNodeConcept> cluster = new ArrayList<>();

        for (int i = 0; i < oldChildren.size(); i++) {

            // Add to cluster
            if (!(oldChildren.get(i) instanceof OntologyNodeModifier)) {
                cluster.add(oldChildren.get(i));
            } else {
                newModifiers.add(oldChildren.get(i));
            }

            // Merge cluster
            if (cluster.size() == fanout) {
                newChildren.add(merge(cluster));
                cluster.clear();
            }
        }

        // Merge cluster
        if (!newChildren.isEmpty() && !cluster.isEmpty()) {
            newChildren.add(merge(cluster));
        }

        // If merging happened
        if (!newChildren.isEmpty()) {
            this.children.clear();
            this.children.addAll(newModifiers);
            this.children.addAll(newChildren);
            return true;

        } else {
            return false;
        }
    }

    /**
     * Serialize as XML
     * 
     * @param writer
     * @param prefix
     * @throws IOException
     */
    private void serialize(BufferedWriter writer, String prefix) throws IOException {
        String _path = StringEscapeUtils.escapeXml(getTarget(this));
        String _code = StringEscapeUtils.escapeXml(getCode());
        String _name = StringEscapeUtils.escapeXml(getName());
        writer.write(prefix);
        writer.write("<node type=\"" + getType(this) + "\" name=\"" + _name + "\" hide=\"" +
                     this.hide + "\" attribute=\"" + this.attribute + "\" code=\"" + _code +
                     "\" numeric=\"" + this.numeric + "\" target=\"" + _path + "\">\n");
        for (OntologyNodeConcept child : children) {
            child.serialize(writer, prefix + "   ");
        }
        writer.write(prefix);
        writer.write("</node>\n");
    }

    /**
     * Recursively sorts all nodes lexicographically
     */
    private void sort() {
        Collections.sort(this.children, new Comparator<OntologyNodeConcept>() {
            @Override
            public int compare(OntologyNodeConcept o1, OntologyNodeConcept o2) {
                boolean m1 = (o1 instanceof OntologyNodeModifier);
                boolean m2 = (o2 instanceof OntologyNodeModifier);
                return (m1 && !m2) ? -1 : ((!m1 && m2) ? 1 : o1.name.compareTo(o2.name));
            }
        });
        for (OntologyNodeConcept child : children) {
            child.sort();
        }
    }

    /**
     * To string
     * 
     * @param builder
     * @param prefix
     */
    private void toString(StringBuilder builder, String prefix) {
        builder.append(prefix);
        builder.append("Node (type=" + getType(this) + ", name=" + this.getName() + ", code=" +
                       this.getCode() + ", numeric=" + this.numeric + ")\n");
        for (OntologyNodeConcept child : children) {
            child.toString(builder, prefix + "   ");
        }
    }
}
