/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;

/**
 * Simple file utils
 * 
 * TODO: Replace with something mature, e.g. from Apache Commons
 */
public class FileUtil {

    /** To fix problems with case sensitivity on Windows */
    private static final Set<String>         FILENAMES          = new HashSet<>();
    /** To fix problems with case sensitivity on Windows */
    private static final Map<String, String> ENTITY_TO_FILENAME = new HashMap<>();

    /**
     * Creates a name for a specific categorical value
     * 
     * @param attributeName
     * @param value
     * @return
     */
    public static String escapeColumnName(String attributeName, String value) {
        return attributeName + "_" + value;
    }

    /**
     * Encode entity and concept into legal filename. Does not support escaping
     * path names!
     * 
     * @param entity
     * @param concept
     * @return
     */
    public static String escapeFileName(ConfigurationInputFile entity, String concept) {

        // Get stored filename
        String name = entity.getName() + "_" + concept;
        String result = ENTITY_TO_FILENAME.get(name);

        // Calculate if not found
        if (result == null) {
            result = escapeFileName(name) + ".csv";
            while (FILENAMES.contains(result)) {
                result = "_" + result;
            }
            ENTITY_TO_FILENAME.put(name, result);
            FILENAMES.add(result);
        }

        // Done
        return result;
    }

    /**
     * Escapes the filename
     * 
     * @param filename
     * @return
     */
    public static String escapeFileName(String filename) {
        try {
            filename = URLEncoder.encode(filename, "UTF-8").replace("*", "_");
        } catch (UnsupportedEncodingException e) {
            filename = filename.replace("\\", " resp. ");
            filename = filename.replace("/", " resp. ");
            filename = filename.replaceAll("[^a-zA-Z0-9\\.\\-]", "_");
        }
        return filename.toLowerCase();
    }
}
